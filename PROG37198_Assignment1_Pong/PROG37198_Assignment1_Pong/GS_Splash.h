#pragma once
#include "AGameState.h"

#include <SFML/Graphics/VertexArray.hpp>
#include "MenuItem.h"

class GS_Splash :
	public AGameState
{
GS_SINGLETON(GS_Splash)
public:

	// Inherited via AGameState
	virtual void init() override;
	virtual void deinit() override;
	virtual void pause() override;
	virtual void resume() override;
	virtual bool handleEvent(sf::Event const &) override;
	virtual void update(float delta) override;

protected:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const override;
private:
	std::pair<sf::VertexArray, sf::Transform> _splashText;
	sf::Vector2f _splashTextVelocity;
	sf::Vector2f _splashTextPosition;
	float mCurrentFadeTime;
	bool mCurrentlyFading;
	float const FADE_TIME;
	sf::Color const TARGET_COLOR;
	MenuItem _startButton;
	const void startButton(MenuItem &item);
	std::vector<std::pair<sf::VertexArray, sf::Transform>> mGroupMembers;
	std::vector<std::pair<sf::VertexArray, sf::Transform>> mCredits;
};


#pragma once
#include "AGameState.h"
#include "MenuItem.h"
#include <vector>

namespace sf {
	class VertexArray;
}
class GS_MainMenu : public AGameState
{
	GS_SINGLETON(GS_MainMenu);
public:
	virtual void init() override;
	virtual void deinit() override;
	virtual void pause() override;
	virtual void resume() override;
	virtual bool handleEvent(sf::Event const &) override;
	virtual void update(float delta) override;
protected:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const override;
private:
	std::vector<std::pair<sf::VertexArray, sf::Transform>> _menuText;
	std::vector<MenuItem> _menuButtons;
};

#pragma once
#include "MenuItem.h"
#include <SFML/Graphics.hpp>
class MenuSlider;
class MenuSlider :
	public MenuItem
{
public:
	MenuSlider(float width, float height, sf::Color color);
	~MenuSlider();
	float getSliderValue();
	void registerLessCallback(const cb_t &callback);
	void registerMoreCallback(const cb_t &callback);
	void unregisterCallbacks();
	void handleEvents(sf::Event const &ev);
	//Had to make public so window can draw them...
	MenuItem lessButton;
	MenuItem moreButton;
	void setPosition(sf::Vector2f &position);
	void setPosition(float x, float y);
	float getWidth();
private:
	float _sliderValue;
	float _width;
	float _height;
	sf::FloatRect _bounds;
	sf::RectangleShape _slider;
	void _plus(MenuItem &caller);
	void _minus(MenuItem &caller);
protected:
	// Inherited via Drawable
	virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const override;
};


#pragma once

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Vertex.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Audio.hpp>

#include <string>
#include <vector>
#include <map>

#define SOUNDSTARTVOLUME 50
#define SOUNDMAXVOLUME 100

//Mapping for which sound effect will play
enum SoundEffect {
	 BLIP = 0,
	 JUMP,
	 RING,
	 COUNT,
};

//Flag for which music is playing, incrementing by 2 pow X. IE song 3 would equal 4 so multiple songs can play at once when set in the variable. IE 3 would play songs 1 and 2 (Bit wise AND)
enum PlayingMusic {
	NONE=0,
	SONG1=1,
	SONG2=2,
	ALL
};

class AssetManager
{
	//Singleton stuff
public:
	static AssetManager &getInstance() {
		static AssetManager instance;
		return instance;
	}
private:
	AssetManager();
	~AssetManager();
	//Leave these unimplemented, prevents copying of
	// the singleton.
	AssetManager(AssetManager const &);
	void operator=(AssetManager const &);
	int _playingMusic;
	float _sfxVolume;
	float _musicVolume;
	sf::Time _musicAt;
	sf::Time getCurrentPlayTime(std::vector<sf::Music *> &music);
	sf::Time getCurrentPlayMaxTime(std::vector<sf::Music *> &music);
public:
	sf::VertexArray getSprite(std::string const &name);
	sf::VertexArray getText(std::string const &istr, float cheight = 4.0f);
	PlayingMusic getCurrentlyPlayingFlags();
	void startMusic(PlayingMusic song);
	void stopMusic(PlayingMusic song);
	void playSound(SoundEffect sound);
	void stopAllSounds();

	void setSfxVolume(float value);
	float getSfxVolume();
	void setMusicVolume(float value);
	float getMusicVolume();
	sf::Time getCurrentPlayingTime();

private:
	std::map<std::string, std::vector<sf::Vector2i>> mSprites;
	std::map<char, std::vector<sf::Vector2i>> mChars;
	std::vector<sf::Music *> _music;
	std::vector<sf::SoundBuffer *> _soundBuffers;
	std::vector<sf::Sound> _sounds;
};

extern AssetManager *gAssetManager;


#pragma once
#include "AGameState.h"
#include <SFML/Graphics.hpp>
#include <vector>
namespace sf {
	class VertexArray;
	class Transform;
}
class GS_Pong :
	public AGameState
{
	//Singleton-a-fy
	GS_SINGLETON(GS_Pong)

public:

	// Inherited via AGameState
	virtual void init() override;
	virtual void deinit() override;
	virtual void pause() override;
	virtual void resume() override;
	virtual bool handleEvent(sf::Event const &) override;
	virtual void update(float delta) override;

	// Gameplay functions
	void resetGame(bool startGame = false);
	void setBall(bool isShadowBall = false);
	void moveBall(float delta);
	void movePaddle(int paddleIndex, float delta, sf::Keyboard::Key up, sf::Keyboard::Key down);
	void moveAIPaddle(float delta);
	void setAIDifficulty(int difficulty);
	int getAIDifficulty();
	void updateScoreboard(bool reset = false);
	void updateWinner(int playerIndex);

protected:
	// Inherited via sf::Drawable by way of AGameState
	virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const override;
	
private:
	std::vector<std::tuple<sf::RectangleShape, sf::Vector2f, sf::Vector3f, bool>> mBalls;
	std::vector<sf::RectangleShape> mPaddles;
	std::vector<std::tuple<sf::VertexArray, sf::Transform, std::int32_t>> mPlayerScores;
	std::vector<std::tuple<sf::VertexArray, sf::Transform, std::int32_t>> mAIDifficultyDisplay;
	std::vector<std::pair<sf::VertexArray, sf::Transform>> mGameMessages;
	bool mPaused;
	bool mGameInProgress;
	bool mSinglePlay;
	float mResetDoubleTap;
	float mSetBallTimer;
	int mNextBallDirection;
	std::vector<float> mTextFlashTimers;
};

#pragma once

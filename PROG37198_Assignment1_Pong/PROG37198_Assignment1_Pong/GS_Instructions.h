#pragma once
#include "AGameState.h"
#include "GameStateManager.h"
#include "MenuItem.h"

#include <SFML/Graphics.hpp>

class GS_Instructions : public AGameState
{
	GS_SINGLETON(GS_Instructions);
public:


	// Inherited via AGameState
	
	virtual void init() override;
	virtual void deinit() override;
	virtual void pause() override;
	virtual void resume() override;
	virtual bool handleEvent(sf::Event const &) override;
	virtual void update(float delta) override;
protected:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const override;
private:
	sf::RectangleShape _background;
	std::vector<std::pair<sf::VertexArray, sf::Transform>> _menuText;
	std::vector<MenuItem> _menuButtons;
};


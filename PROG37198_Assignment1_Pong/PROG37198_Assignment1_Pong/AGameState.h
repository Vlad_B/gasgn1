#pragma once

#include <SFML/Graphics/Drawable.hpp>
#define GS_SINGLETON(CLASS_NAME)	\
	public:						\
	static CLASS_NAME &getInstance() {	\
		static CLASS_NAME instance;		\
		return instance;				\
	}									\
	private:							\
	CLASS_NAME(); 						\
	~CLASS_NAME();						\
	CLASS_NAME(CLASS_NAME const &);		\
	void operator=(CLASS_NAME const &);		


namespace sf {
	class Event;
}

class AGameState : public sf::Drawable
{
protected:
	AGameState(bool opaque = true) : mOpaque(opaque) {}
	virtual ~AGameState() {}

public:

	//Note that init() and deinit() must be safe
	// to call multiple times in a row i.e. call init()
	// more than once without an intervening deinit()
	// (and vice versa) if you want to implement your
	// gamestates as singletons AND you plan on having
	// the same instance in the stack multiple times.
	// If you only have the state in the stack once
	// and you never call init() and deinit() outside
	// of the gamestatemanager code you should be ok
	// (for a certain definition of should)
	virtual void init() = 0;
	virtual void deinit() = 0;
	virtual void pause() = 0;
	virtual void resume() = 0;
	//Why return true? We could have the state consume
	// the event, and in the event that it isn't consumed
	// pass the event to the next state in the stack.
	//Ignored for now.
	virtual bool handleEvent(sf::Event const &) = 0;
	virtual void update(float delta) = 0;
	//Note that we don't need to declare a draw prototype because
	// we are inheriting from sf::Drawable. We could conceivably
	// also have an IUpdatable and an IEventHandler interface
	// as well.

	virtual bool isOpaque() {
		return mOpaque;
	}
	virtual void setOpaque(bool opaque) {
		mOpaque = opaque;
	}

private:
	bool mOpaque;
};
#define _USE_MATH_DEFINES
#include <math.h>

#include "GS_Pong.h"
//#include "GS_PauseMenu.h"
#include "GameStateManager.h"
#include "AssetManager.h"
#include "GS_PauseMenu.h"

#include <vector>

#if _DEBUG
#include <iostream>
#endif

#define BALL_SIZE 10
#define PADDLE_HEIGHT 48
#define PADDLE_START_SPEED 240
#define BALL_START_SPEED 200
#define PADDLE_SPEED_INCREMENT 40
#define PADDLE_X_OFFSET 20

#define BACKGROUND_COLOR sf::Color::Black
#define START_BUTTON sf::Keyboard::Enter
#define PAUSE_BUTTON sf::Keyboard::Escape
#define AI_BUTTON sf::Keyboard::I
#define RESET_BUTTON sf::Keyboard::R

#define BALL_COLOR sf::Color::White
#if _DEBUG
#define SHADOW_BALL_COLOR sf::Color::Color(60,60,60)
#else
#define SHADOW_BALL_COLOR BALL_COLOR
#endif
#define BALL_MAX_START_ANGLE 65
#define BALL_SET_TIME 1.0f
#define TEXT_FLASH_FREQUENCY 300
#define TEXT_FLASH_TIME 2.0f

#define AI_COLOR sf::Color::Yellow
#define AI_MAX_LEVEL 10

#define PLAYER_1_COLOR sf::Color::Cyan
#define PLAYER_1_INDEX 0
#define PLAYER_1_UP sf::Keyboard::W
#define PLAYER_1_DOWN sf::Keyboard::S

#define PLAYER_2_COLOR sf::Color::Red
#define PLAYER_2_INDEX 1
#define PLAYER_2_UP sf::Keyboard::Up
#define PLAYER_2_DOWN sf::Keyboard::Down

GS_Pong::GS_Pong() 
	: mPaused(false)
	, mGameInProgress(false)
	, mSinglePlay(false)
	, mResetDoubleTap(-1.0f)
	, mSetBallTimer(-1.0f)
	, mNextBallDirection(0)
{
}

GS_Pong::~GS_Pong()
{
}

void GS_Pong::init()
{
// Commented out lines were for testing purposes
//#if _DEBUG
//	std::srand(10);
//#else
	std::srand(std::time(NULL));
//#endif
	// clear paddles and balls
	mBalls.clear();
	mPaddles.clear();
	
	// setup paddles
	mPaddles.emplace_back(sf::Vector2f(BALL_SIZE, PADDLE_HEIGHT));
	mPaddles.back().setFillColor(PLAYER_1_COLOR);
	mPaddles.back().setOrigin(0, PADDLE_HEIGHT * 0.5f);
	mPaddles.back().setPosition(PADDLE_X_OFFSET, SCREEN_HEIGHT * 0.5f);
	mPaddles.emplace_back(sf::Vector2f(BALL_SIZE, PADDLE_HEIGHT));
	mPaddles.back().setFillColor((mSinglePlay) ? AI_COLOR : PLAYER_2_COLOR );
	mPaddles.back().setOrigin(BALL_SIZE, PADDLE_HEIGHT * 0.5f);
	mPaddles.back().setPosition(SCREEN_WIDTH - PADDLE_X_OFFSET, SCREEN_HEIGHT * 0.5f);

	// initialize timers for text flash
	mTextFlashTimers.emplace_back(-1.0f);	// Player 1 Score
	mTextFlashTimers.emplace_back(-1.0f);	// Player 2 Score
	mTextFlashTimers.emplace_back(-1.0f);	// AI Difficulty Level

	setAIDifficulty(0);
	resetGame();
	
#if _DEBUG
	std::cout << "GS_Pong::init complete" << std::endl;
#endif
}

void GS_Pong::deinit()
{
	// clear
	mBalls.clear();
	mPaddles.clear();
	mPlayerScores.clear();
	mAIDifficultyDisplay.clear();
	mTextFlashTimers.clear();
#if _DEBUG
	std::cout << "GS_Pong::deinit complete" << std::endl;
#endif
}

void GS_Pong::pause()
{
	mPaused = true;
}

void GS_Pong::resume()
{
	mPaused = false;
}

bool GS_Pong::handleEvent(sf::Event const &ev)
{
	if (ev.type == sf::Event::KeyReleased) {
		// START GAME
		if (ev.key.code == START_BUTTON) {
			if (!mGameInProgress) {
				resetGame(true);
			}
		}
		// PAUSE
		if (ev.key.code == PAUSE_BUTTON) {
#if _DEBUG
			std::cout << "GS_Pong::PAUSE_BUTTON" << std::endl;
#endif
			if (!mPaused) {
				GameStateManager::getInstance().push(GS_PauseMenu::getInstance());
			}
#if _DEBUG
			std::cout << "GS_Pong::Pause was toggled to " << std::to_string(mPaused) << "" << std::endl;
#endif
		}
		// RESET
		if (ev.key.code == RESET_BUTTON) {
#if _DEBUG
			std::cout << "GS_Pong::RESET_BUTTON" << std::endl;
#endif
			if (mResetDoubleTap > 0) {
				// reset the counter so it doesn't trigger again
				mResetDoubleTap = -1.0f;
				resetGame();
#if _DEBUG
				std::cout << "GS_Pong::Game Reset!" << std::endl;
#endif
			}
			else {
				// set 2 second timer
				mResetDoubleTap = 2.0f;
			}
		}
		// TOGGLE AI
		if (ev.key.code == AI_BUTTON) {
#if _DEBUG
			std::cout << "GS_Pong::AI_BUTTON" << std::endl;
#endif
			// toggle single player mode
			mSinglePlay = !mSinglePlay;
			// toggle between ai and player 2 color
			mPaddles.back().setFillColor((mSinglePlay) ? AI_COLOR : PLAYER_2_COLOR );
			updateScoreboard();
#if _DEBUG
			std::cout << "GS_Pong::AI was toggled to " << std::to_string(mSinglePlay) << "" << std::endl;
#endif
		}
		// SET AI DIFFICULTY
		if (mSinglePlay && ev.key.code == PLAYER_2_UP) {
#if _DEBUG
			std::cout << "GS_Pong::PLAYER_2_UP" << std::endl;
#endif
			// increment difficulty
			setAIDifficulty(getAIDifficulty() + 1);
			// set the ai difficulty text timer
			mTextFlashTimers[2] = TEXT_FLASH_TIME;
		}
		if (mSinglePlay && ev.key.code == PLAYER_2_DOWN) {
#if _DEBUG
			std::cout << "GS_Pong::PLAYER_2_DOWN" << std::endl;
#endif
			// decrement difficulty
			setAIDifficulty(getAIDifficulty() - 1);
			// set the ai difficulty text timer
			mTextFlashTimers[2] = TEXT_FLASH_TIME;
		}
	}
	return true;
}

void GS_Pong::update(float delta)
{
	// When the pause menu is toggled, we don't want to update this state!
	if (mPaused) {
		return;
	}

	// Countdown the reset timer to zero
	if (mResetDoubleTap > 0) {
		mResetDoubleTap -= delta;
	}

	// move player 1 paddle
	movePaddle(PLAYER_1_INDEX, delta, PLAYER_1_UP, PLAYER_1_DOWN);

	if (!mSinglePlay) {
		// move player 2 paddle
		movePaddle(PLAYER_2_INDEX, delta, PLAYER_2_UP, PLAYER_2_DOWN);
	}
	else {
		// let the ai move player 2 paddle
		moveAIPaddle(delta);
	}

	// do this only while a game is in progress
	if (mGameInProgress) {
		// countdown the set ball timer to zero
		if (mSetBallTimer > 0) {
			mSetBallTimer -= delta;
			if (mSetBallTimer <= 0) {
				// set a new ball once it reaches zero
				setBall();
			}
		}
		
		// move the balls
		moveBall(delta);

	}

	for (float &timer : mTextFlashTimers) {
		// countdown each text flash timer to zero
		if (timer > 0) {
			timer -= delta;
			if (timer <= 0 && !mGameInProgress) {
				timer += TEXT_FLASH_TIME;
			}
		}
	}
}

void GS_Pong::resetGame(bool startGame)
{
	AssetManager::getInstance().stopAllSounds();
	// clear balls
	mBalls.clear();
	// reset paddle position
	mPaddles[PLAYER_1_INDEX].setPosition(PADDLE_X_OFFSET, SCREEN_HEIGHT * 0.5f);
	mPaddles[PLAYER_2_INDEX].setPosition(SCREEN_WIDTH - PADDLE_X_OFFSET, SCREEN_HEIGHT * 0.5f);
	// reset the scoreboard
	updateScoreboard(true);
	// if startGame is true, it will reset and begin a new game immediately. Otherwise it will go back to its initial state
	mGameInProgress = startGame;
	// the first ball is only set when a game has been started
	mSetBallTimer = (startGame) ? BALL_SET_TIME : -1.0f ;
	// the first ball's direction is random
	mNextBallDirection = 0;
	// reset text flash timers
	for (float &timer : mTextFlashTimers) {
		timer = -1.0f;
	}
	// remove any game messages
	mGameMessages.clear();

	if (!startGame) {
		AssetManager &assets = AssetManager::getInstance();
		// add a message if the game was not started
		mGameMessages.emplace_back(assets.getText("Press Enter to start a new game.", 15), sf::Transform());
		std::get<1>(mGameMessages.back()).translate(SCREEN_WIDTH * 0.3f, SCREEN_HEIGHT * 0.3f);
		for (std::size_t i = 0; i < std::get<0>(mGameMessages.back()).getVertexCount(); ++i) {
			std::get<0>(mGameMessages.back())[i].color = sf::Color::White;
			std::get<0>(mGameMessages.back())[i].position.x += SCREEN_WIDTH * 0.2f;
			std::get<0>(mGameMessages.back())[i].position.y += SCREEN_HEIGHT * 0.3f + 45 + (mGameMessages.size() * 45);
		}
	}
}

void GS_Pong::setBall(bool isShadowBall)
{
	// create a new ball
	mBalls.emplace_back(sf::Vector2f(BALL_SIZE, BALL_SIZE), sf::Vector2f(0.0f, 0.0f), sf::Vector3f(0.0f, 0.0f, 0.0f), isShadowBall);
	sf::RectangleShape &ball = std::get<0>(mBalls.back());
	sf::Vector2f &vel = std::get<1>(mBalls.back());
	// in debug mode you can see which ball is the shadow ball
	std::get<0>(mBalls.back()).setFillColor((isShadowBall) ? SHADOW_BALL_COLOR : BALL_COLOR );
	std::get<0>(mBalls.back()).setOrigin(std::get<0>(mBalls.back()).getSize() * 0.5f);
	std::get<0>(mBalls.back()).setPosition(SCREEN_WIDTH * 0.5f, SCREEN_HEIGHT * 0.5f);
	// get a random angle
	float r = rand() % BALL_MAX_START_ANGLE;
	// if the next ball's x direction is decided, use that direction. Otherwise, select a random direction.
	float x = (mNextBallDirection != 0) ? (float)mNextBallDirection : (rand() % 2 == 0) ? 1.0f : -1.0f;
	// always use a random y direction
	float y = (rand() % 2 == 0) ? 1.0f : -1.0f;
	// set x and y velocity
	std::get<1>(mBalls.back()).x = cos(r*M_PI / 180) * (float)BALL_START_SPEED * x;
	std::get<1>(mBalls.back()).y = sin(r*M_PI / 180) * (float)BALL_START_SPEED * y;
#if _DEBUG
	std::cout << "setupNewBall::r=" << std::to_string(r) << ",v.x=" << std::to_string(std::get<1>(mBalls.back()).x) << ",v.y=" << std::to_string(std::get<1>(mBalls.back()).y) << std::endl;
#endif
	mNextBallDirection = 0;
}

void GS_Pong::moveBall(float delta)
{
	for (auto &ball : mBalls) {
		// Move
		sf::Vector2f oldPos = std::get<0>(ball).getPosition();
		sf::Vector2f oldVel = std::get<1>(ball);
#if _DEBUG
		std::cout << "DEBUG::BALL::oldStats::xPos=" << std::to_string(oldPos.x) << ",yPos=" << std::to_string(oldPos.y) << ",xVel=" << std::to_string(oldVel.x) << ",yVel=" << std::to_string(oldVel.y) << std::endl;
#endif
		std::get<0>(ball).move(std::get<1>(ball) * delta);
		// Wall bounce
		sf::Vector2f newPos = std::get<0>(ball).getPosition();
		if (newPos.y < BALL_SIZE * 0.5f || newPos.y > SCREEN_HEIGHT - (BALL_SIZE * 0.5f)) {
			std::get<0>(ball).move(0.0f, std::get<1>(ball).y * delta * -2.0f);
			std::get<1>(ball).y *= -1.0f;
			AssetManager::getInstance().playSound(SoundEffect::BLIP);
		}
		// Paddle bounce
		sf::FloatRect ballBounds = std::get<0>(ball).getGlobalBounds();
		sf::Vector2f ballCoords = std::get<0>(ball).getPosition();

		// Player 1 paddle
		sf::FloatRect pOnePaddleBounds = mPaddles[PLAYER_1_INDEX].getGlobalBounds();
		// Detect paddle collision
		if (ballBounds.intersects(pOnePaddleBounds)) {
			// Check for top smash
			if (ballCoords.y < pOnePaddleBounds.top) {
				AssetManager::getInstance().playSound(SoundEffect::BLIP);
				std::get<0>(ball).move(std::get<1>(ball).x * delta * -2.0f, 0.0f);
				// Give the ball a big speed boost
				std::get<1>(ball).x *= -1.2f;
				std::get<1>(ball).y -= PADDLE_SPEED_INCREMENT;
			}
			// Check for bottom smash
			else if (ballCoords.y > pOnePaddleBounds.top + pOnePaddleBounds.height) {
				AssetManager::getInstance().playSound(SoundEffect::BLIP);
				std::get<0>(ball).move(std::get<1>(ball).x * delta * -2.0f, 0.0f);
				// Give the ball a big speed boost
				std::get<1>(ball).x *= -1.2f;
				std::get<1>(ball).y += PADDLE_SPEED_INCREMENT;
			}
			// Check segments
			// Note: this part was hardcoded to save time. A future improvement to the code would be to eliminate all the magic values
			// and replace them with defined coefficients. Further improvements would involve calculating the ball's angle from its vector
			// properly, and incorporating that value to simulate proper bounce physics.
			else {
				AssetManager::getInstance().playSound(SoundEffect::BLIP);
				for (std::size_t i = 0; i < 11; i++) {
					float yTop = pOnePaddleBounds.top + ((float)PADDLE_HEIGHT * i / 12.0);
					float yBot = pOnePaddleBounds.top + ((float)PADDLE_HEIGHT * (i + 1) / 12.0);
					// check if the ball's y coord fits within a segment
					if (ballCoords.y >= yTop && ballCoords.y <= yBot) {
						// When the ball hits a particular segment, we try to adjust the x and y velocity based on a hypothetical curve in the paddle
						//		when the ball hits closer to the edge of a paddle, the angle adjustment is greater
						std::get<0>(ball).move(std::get<1>(ball).x * delta * -2.0f, 0.0f);
						std::get<1>(ball).x *= -1.1f * (0.25f + abs((0.2f * i) - 1.0f));
						std::get<1>(ball).y += BALL_START_SPEED * ((0.15f * i) - 0.75f);
						//std::get<1>(ball).y += (PADDLE_SPEED_INCREMENT * 0.2 * (i - 5));
						break;
					}
				}
			}
			// Speed the ball up or slow it down, depending on which direction the paddle was moving
			if (sf::Keyboard::isKeyPressed(PLAYER_1_UP)) {
				std::get<1>(ball).x *= 1.1f;
				std::get<1>(ball).y -= PADDLE_SPEED_INCREMENT;
			}
			if (sf::Keyboard::isKeyPressed(PLAYER_1_DOWN)) {
				std::get<1>(ball).x *= 1.1f;
				std::get<1>(ball).y += PADDLE_SPEED_INCREMENT;
			}
		}
		// Player 2 paddle
		sf::FloatRect pTwoPaddleBounds = mPaddles[PLAYER_2_INDEX].getGlobalBounds();
		if (ballBounds.intersects(pTwoPaddleBounds)) {
			// Check for top smash
			if (ballCoords.y < pTwoPaddleBounds.top) {
				AssetManager::getInstance().playSound(SoundEffect::BLIP);
				std::get<0>(ball).move(std::get<1>(ball).x * delta * -2.0f, 0.0f);
				std::get<1>(ball).x *= -1.2f;
				std::get<1>(ball).y -= PADDLE_SPEED_INCREMENT;
			}
			// Check for bottom smash
			else if (ballCoords.y > pTwoPaddleBounds.top + pTwoPaddleBounds.height) {
				AssetManager::getInstance().playSound(SoundEffect::BLIP);
				std::get<0>(ball).move(std::get<1>(ball).x * delta * -2.0f, 0.0f);
				std::get<1>(ball).x *= -1.2f;
				std::get<1>(ball).y += PADDLE_SPEED_INCREMENT;
			}
			else {
				AssetManager::getInstance().playSound(SoundEffect::BLIP);
				for (std::size_t i = 0; i < 11; i++) {
					float yTop = pTwoPaddleBounds.top + ((float)PADDLE_HEIGHT * i / 12.0);
					float yBot = pTwoPaddleBounds.top + ((float)PADDLE_HEIGHT * (i + 1) / 12.0);
					if (ballCoords.y >= yTop && ballCoords.y <= yBot) {
						std::get<0>(ball).move(std::get<1>(ball).x * delta * -2.0f, 0.0f);
						std::get<1>(ball).x *= -1.1f * (0.25f + abs((0.2f * i) - 1.0f));
						std::get<1>(ball).y += BALL_START_SPEED * ((0.15f * i) - 0.75f);
						//std::get<1>(ball).y += (PADDLE_SPEED_INCREMENT * 0.2 * (i - 5));
						break;
					}
				}
			}
			if (sf::Keyboard::isKeyPressed(PLAYER_2_UP)) {
				std::get<1>(ball).x *= 1.1f;
				std::get<1>(ball).y -= PADDLE_SPEED_INCREMENT;
			}
			if (sf::Keyboard::isKeyPressed(PLAYER_2_DOWN)) {
				std::get<1>(ball).x *= 1.1f;
				std::get<1>(ball).y += PADDLE_SPEED_INCREMENT;
			}
		}
#if _DEBUG
		std::cout << "DEBUG::BALL::oldStats::xPos=" << std::to_string(std::get<0>(ball).getPosition().x) << ",yPos=" << std::to_string(std::get<0>(ball).getPosition().y) << ",xVel=" << std::to_string(std::get<1>(ball).x) << ",yVel=" << std::to_string(std::get<1>(ball).y) << std::endl;
#endif
		// Scoring
		if (!std::get<3>(ball) && std::get<0>(ball).getPosition().x < PADDLE_X_OFFSET + BALL_SIZE - 1) {
			// Player 2 scores
			std::get<2>(mPlayerScores[PLAYER_2_INDEX])++;
			AssetManager::getInstance().playSound((std::get<2>(mPlayerScores[PLAYER_2_INDEX]) >= 9) ? SoundEffect::RING : SoundEffect::JUMP );
#if _DEBUG
			std::cout << "main::Update::Player 2 scores!    " << std::to_string(std::get<2>(mPlayerScores[PLAYER_1_INDEX])) << " - " << std::to_string(std::get<2>(mPlayerScores[PLAYER_2_INDEX])) << std::endl;
#endif
			// Set flash timer
			mTextFlashTimers[PLAYER_2_INDEX] = TEXT_FLASH_TIME;
			updateScoreboard();
			// prepare next ball
			mSetBallTimer = BALL_SET_TIME;
			// record the direction of the next ball - 1 = positive x, -1 = negative x
			mNextBallDirection = 1;
		}
		if (!std::get<3>(ball) && std::get<0>(ball).getPosition().x > SCREEN_WIDTH - PADDLE_X_OFFSET - BALL_SIZE + 1) {
			// Player 1 scores
			std::get<2>(mPlayerScores[PLAYER_1_INDEX])++;
			AssetManager::getInstance().playSound((std::get<2>(mPlayerScores[PLAYER_1_INDEX]) >= 9) ? SoundEffect::RING : SoundEffect::JUMP);
#if _DEBUG
			std::cout << "main::Update::Player 1 scores!    " << std::to_string(std::get<2>(mPlayerScores[PLAYER_1_INDEX])) << " - " << std::to_string(std::get<2>(mPlayerScores[PLAYER_2_INDEX])) << std::endl;
#endif
			mTextFlashTimers[PLAYER_1_INDEX] = TEXT_FLASH_TIME;
			updateScoreboard();
			mSetBallTimer = BALL_SET_TIME;
			mNextBallDirection = -1;
		}
	}
	// Winner
	if (std::get<2>(mPlayerScores[PLAYER_1_INDEX]) >= 9) {
		// stop the game
		mGameInProgress = false;
		// clear the balls
		mBalls.clear();
		updateWinner(PLAYER_1_INDEX);
	}
	if (std::get<2>(mPlayerScores[PLAYER_2_INDEX]) >= 9) {
		mGameInProgress = false;
		mBalls.clear();
		updateWinner(PLAYER_2_INDEX);
	}
	// Cleanup
	for (std::size_t i = mBalls.size(); i > 0; --i) {
		if (!std::get<3>(mBalls[i - 1]) && std::get<0>(mBalls[i - 1]).getPosition().x < PADDLE_X_OFFSET + BALL_SIZE - 1) {
			mBalls.erase(mBalls.begin() + i - 1);
			continue;
		}
		if (!std::get<3>(mBalls[i - 1]) && std::get<0>(mBalls[i - 1]).getPosition().x > SCREEN_WIDTH - PADDLE_X_OFFSET - BALL_SIZE + 1) {
			mBalls.erase(mBalls.begin() + i - 1);
			continue;
		}
		if (std::get<3>(mBalls[i - 1]) && std::get<0>(mBalls[i - 1]).getPosition().x < -PADDLE_X_OFFSET) {
			mBalls.erase(mBalls.begin() + i - 1);
			continue;
		}
		if (std::get<3>(mBalls[i - 1]) && std::get<0>(mBalls[i - 1]).getPosition().x > SCREEN_WIDTH + PADDLE_X_OFFSET) {
			mBalls.erase(mBalls.begin() + i - 1);
			continue;
		}
	}
}

void GS_Pong::movePaddle(int paddleIndex, float delta, sf::Keyboard::Key up, sf::Keyboard::Key down)
{
	sf::Vector2f pos = mPaddles[paddleIndex].getPosition();
	if (sf::Keyboard::isKeyPressed(up) && sf::Keyboard::isKeyPressed(down)) {
		return;
	}
	if (sf::Keyboard::isKeyPressed(up)) {
		pos.y -= PADDLE_START_SPEED * delta;
	}
	else if (sf::Keyboard::isKeyPressed(down)) {
		pos.y += PADDLE_START_SPEED * delta;
	}
	if (pos.y < PADDLE_HEIGHT * 0.5f) {
		pos.y = PADDLE_HEIGHT * 0.5f;
	}
	if (pos.y > SCREEN_HEIGHT - (PADDLE_HEIGHT * 0.5f)) {
		pos.y = SCREEN_HEIGHT - (PADDLE_HEIGHT * 0.5f);
	}
	mPaddles[paddleIndex].setPosition(pos);
}

void GS_Pong::moveAIPaddle(float delta)
{
	// Very simple AI
	// The AI locks onto a target and moves to match that target's y position
	//		if there is a ball, the AI follows the position of the first ball in the vector
	//		if there is no ball, the AI will quickly recenter itself to prepare for the next volley
	//		if the game is not in progress, the AI will match the position of player 1's paddle
	sf::Vector2f pos = mPaddles[PLAYER_2_INDEX].getPosition();
	sf::Vector2f target = (!mGameInProgress) ? mPaddles[PLAYER_1_INDEX].getPosition() : (mBalls.size() > 0) ? std::get<0>(mBalls.front()).getPosition() : sf::Vector2f(SCREEN_WIDTH * 0.5f, SCREEN_HEIGHT * 0.5f);
	// The speed at which the AI follows its target is determined by it's level
	//		the lower the level, the slower the paddle moves
	//		in this way, it is still capped at the same speed as the player paddle
	if (pos.y > target.y) {
		pos.y -= PADDLE_START_SPEED * delta * (getAIDifficulty() + 1) / (AI_MAX_LEVEL + 1);
		if (pos.y < target.y) {
			pos.y = target.y;
		}
	}
	else if (pos.y < target.y) {
		pos.y += PADDLE_START_SPEED * delta * (getAIDifficulty() + 1) / (AI_MAX_LEVEL + 1);
		if (pos.y > target.y) {
			pos.y = target.y;
		}
	}
	// Note: The AI is very weak. It does not have the ability to predict the future position of the ball, and
	// so it frequently leaves itself open on low levels. 
	mPaddles[PLAYER_2_INDEX].setPosition(pos);
}

void GS_Pong::setAIDifficulty(int difficulty)
{
	int aiDifficulty = difficulty;
	// clamp the difficulty between zero and the max level
	if (aiDifficulty > AI_MAX_LEVEL) {
		aiDifficulty = AI_MAX_LEVEL;
	}
	if (aiDifficulty < 0) {
		aiDifficulty = 0;
	}
	AssetManager &assets = AssetManager::getInstance();
	// reset the ai difficulty display
	mAIDifficultyDisplay.clear();
	mAIDifficultyDisplay.emplace_back(assets.getText("AI LEVEL " + std::to_string(aiDifficulty), 20), sf::Transform(), aiDifficulty);
	for (std::size_t i = 0; i < std::get<0>(mAIDifficultyDisplay.back()).getVertexCount(); ++i) {
		std::get<0>(mAIDifficultyDisplay.back())[i].color = AI_COLOR;
		std::get<0>(mAIDifficultyDisplay.back())[i].position.x += SCREEN_WIDTH * 0.65f;
		std::get<0>(mAIDifficultyDisplay.back())[i].position.y += 25.0f;
	}
#if _DEBUG
	std::cout << "GS_Pong::AI level set to " << std::to_string(std::get<2>(mAIDifficultyDisplay.back())) << "" << std::endl;
#endif
}

int GS_Pong::getAIDifficulty()
{
	return std::get<2>(mAIDifficultyDisplay.back());
}

void GS_Pong::updateScoreboard(bool reset)
{
	AssetManager &assets = AssetManager::getInstance();
	if (reset) {
		mPlayerScores.clear();
		mPlayerScores.emplace_back(assets.getText("0", 40), sf::Transform(), 0);
		std::get<1>(mPlayerScores.back()).translate(SCREEN_WIDTH * 0.3f, SCREEN_HEIGHT * 0.3f);
		for (std::size_t i = 0; i < std::get<0>(mPlayerScores.back()).getVertexCount(); ++i) {
			std::get<0>(mPlayerScores.back())[i].color = PLAYER_1_COLOR;
			std::get<0>(mPlayerScores.back())[i].position.x += SCREEN_WIDTH * 0.3f;
			std::get<0>(mPlayerScores.back())[i].position.y += SCREEN_HEIGHT * 0.3f;
		}
		mPlayerScores.emplace_back(assets.getText("0", 40), sf::Transform(), 0);
		std::get<1>(mPlayerScores.back()).translate(SCREEN_WIDTH * 0.7f, SCREEN_HEIGHT * 0.3f);
		for (std::size_t i = 0; i < std::get<0>(mPlayerScores.back()).getVertexCount(); ++i) {
			std::get<0>(mPlayerScores.back())[i].color = (mSinglePlay) ? AI_COLOR : PLAYER_2_COLOR ;
			std::get<0>(mPlayerScores.back())[i].position.x += SCREEN_WIDTH * 0.65f;
			std::get<0>(mPlayerScores.back())[i].position.y += SCREEN_HEIGHT * 0.3f;
		}
	}
	else {
		mPlayerScores.emplace_back(assets.getText(std::to_string(std::get<2>(mPlayerScores[0])), 40), sf::Transform(), std::get<2>(mPlayerScores[0]));
		std::get<1>(mPlayerScores.back()).translate(SCREEN_WIDTH * 0.3f, SCREEN_HEIGHT * 0.3f);
		for (std::size_t i = 0; i < std::get<0>(mPlayerScores.back()).getVertexCount(); ++i) {
			std::get<0>(mPlayerScores.back())[i].color = PLAYER_1_COLOR;
			std::get<0>(mPlayerScores.back())[i].position.x += SCREEN_WIDTH * 0.3f;
			std::get<0>(mPlayerScores.back())[i].position.y += SCREEN_HEIGHT * 0.3f;
		}
		mPlayerScores.emplace_back(assets.getText(std::to_string(std::get<2>(mPlayerScores[1])), 40), sf::Transform(), std::get<2>(mPlayerScores[1]));
		std::get<1>(mPlayerScores.back()).translate(SCREEN_WIDTH * 0.7f, SCREEN_HEIGHT * 0.3f);
		for (std::size_t i = 0; i < std::get<0>(mPlayerScores.back()).getVertexCount(); ++i) {
			std::get<0>(mPlayerScores.back())[i].color = (mSinglePlay) ? AI_COLOR : PLAYER_2_COLOR;
			std::get<0>(mPlayerScores.back())[i].position.x += SCREEN_WIDTH * 0.65f;
			std::get<0>(mPlayerScores.back())[i].position.y += SCREEN_HEIGHT * 0.3f;
		}
		mPlayerScores.erase(mPlayerScores.begin(), mPlayerScores.begin() + 2);
	}
}

void GS_Pong::updateWinner(int playerIndex)
{
	mGameMessages.clear();
	int num = (playerIndex == PLAYER_1_INDEX) ? 1 : 2;
	AssetManager &assets = AssetManager::getInstance();
	// add a message that tells who won the game
	mGameMessages.emplace_back(assets.getText("Player " + std::to_string(num) + " wins", 30), sf::Transform());
	std::get<1>(mGameMessages.back()).translate(SCREEN_WIDTH * 0.3f, SCREEN_HEIGHT * 0.3f);
	for (std::size_t i = 0; i < std::get<0>(mGameMessages.back()).getVertexCount(); ++i) {
		std::get<0>(mGameMessages.back())[i].color = (playerIndex == PLAYER_1_INDEX) ? PLAYER_1_COLOR : (mSinglePlay) ? AI_COLOR : PLAYER_2_COLOR ;
		std::get<0>(mGameMessages.back())[i].position.x += SCREEN_WIDTH * 0.27f;
		std::get<0>(mGameMessages.back())[i].position.y += SCREEN_HEIGHT * 0.3f + 45 + (mGameMessages.size() * 45);
	}
	// add a message that tells how to start the next game
	mGameMessages.emplace_back(assets.getText("Press Enter to start a new game.", 15), sf::Transform());
	std::get<1>(mGameMessages.back()).translate(SCREEN_WIDTH * 0.3f, SCREEN_HEIGHT * 0.3f);
	for (std::size_t i = 0; i < std::get<0>(mGameMessages.back()).getVertexCount(); ++i) {
		std::get<0>(mGameMessages.back())[i].color = sf::Color::White;
		std::get<0>(mGameMessages.back())[i].position.x += SCREEN_WIDTH * 0.2f;
		std::get<0>(mGameMessages.back())[i].position.y += SCREEN_HEIGHT * 0.3f + 45 + (mGameMessages.size() * 45);
	}
}

void GS_Pong::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.clear(sf::Color::Black);
	for (std::size_t i = 0; i < mPaddles.size(); ++i) {
		target.draw(mPaddles[i]);
	}
	for (std::size_t i = 0; i < mBalls.size(); ++i) {
		target.draw(std::get<0>(mBalls[i]));
	}
	for (std::size_t i = 0; i < mPlayerScores.size(); ++i) {
		if (mTextFlashTimers[i % 2] > 0 && (int)(mTextFlashTimers[i % 2] * TEXT_FLASH_FREQUENCY) % 60 < 30) {
			// do not display - the timer controls the flashing text
			// so long as the timer is positive, the text will flash according to the set TEXT_FLASH_FREQUENCY
		}
		else {
			target.draw(std::get<0>(mPlayerScores[i]));
		}
	}
	if (mSinglePlay) {
		for (std::size_t i = 0; i < mAIDifficultyDisplay.size(); ++i) {
			if (mTextFlashTimers[2] > 0 && (int)(mTextFlashTimers[2] * TEXT_FLASH_FREQUENCY) % 60 < 30) {
				// do not display
				// so long as the timer is positive, the text will flash according to the set TEXT_FLASH_FREQUENCY
			}
			else {
				target.draw(std::get<0>(mAIDifficultyDisplay[i]));
			}
		}
	}
	for (std::size_t i = 0; i < mGameMessages.size(); ++i) {
		target.draw(mGameMessages[i].first);
	}
}

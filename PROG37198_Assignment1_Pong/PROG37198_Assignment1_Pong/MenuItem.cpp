#include "MenuItem.h"
#include "AssetManager.h"

#include <SFML/Graphics.hpp>
#include <algorithm>

MenuItem::MenuItem(std::string text, float textHeight, sf::Color bgColor)
	: mTextHeight(textHeight), mDisabled(false), mState(MIS_NORMAL), mForced(false), mForcedState(MIS_NORMAL)
{
	setText(text, true);

}

MenuItem::MenuItem()
	: mTextHeight(100), mDisabled(false), mState(MIS_NORMAL), mForced(false), mForcedState(MIS_NORMAL)
{
	setText("", true);
}

MenuItem::~MenuItem()
{

}

void MenuItem::setText(std::string newText, bool resize)
{
	float const BORDER_PADDING = 10.0f;
	float const UNDERLINE_OFFSET = 2.0f;
	//Clear any existing vertices (we are starting from scratch)
	mVertices.clear();

	//Text vertices setup
	mVertices.push_back(AssetManager::getInstance().getText(newText, mTextHeight));
	//Add offset so that the origin makes sense
	for (std::size_t i = 0; i < mVertices[0].getVertexCount(); ++i) {
		mVertices[0][i].position.x += BORDER_PADDING;
		mVertices[0][i].position.y += BORDER_PADDING;
	}
	mVertices.push_back(mVertices[0]);
	mVertices.push_back(mVertices[0]);
	for (std::size_t i = 0; i < mVertices[0].getVertexCount(); ++i) {
		mVertices[1][i].color = sf::Color(127, 127, 127, 255);
		mVertices[2][i].color = sf::Color::Black;
	}
	sf::FloatRect bounds = mVertices[0].getBounds();

	//Underline
	sf::VertexArray underlineArray;
	underlineArray.setPrimitiveType(sf::Lines);
	underlineArray.resize(2);
	underlineArray[0].color = sf::Color::White;
	underlineArray[0].position.x = bounds.left;
	underlineArray[0].position.y = bounds.top + bounds.height + UNDERLINE_OFFSET;
	underlineArray[1].color = sf::Color::White;
	underlineArray[1].position.x = bounds.left + bounds.width;
	underlineArray[1].position.y = bounds.top + bounds.height + UNDERLINE_OFFSET;
	mVertices.push_back(underlineArray);
	mVertices.push_back(underlineArray);
	mVertices.push_back(underlineArray);
	for (std::size_t i = 0; i < mVertices[3].getVertexCount(); ++i) {
		mVertices[4][i].color = sf::Color(127, 127, 127, 255);
		mVertices[5][i].color = sf::Color::Black;
	}

	//Only redo the borders if a resize is requested.
	if (resize) {
		//Borders setup
		bounds.left -= BORDER_PADDING;
		bounds.top -= BORDER_PADDING;
		bounds.width += BORDER_PADDING * 2.0f;
		bounds.height += BORDER_PADDING * 2.0f;
		mBounds = bounds;
		//Top border
		sf::VertexArray topBorderArray;
		topBorderArray.setPrimitiveType(sf::LinesStrip);
		topBorderArray.resize(3);
		topBorderArray[0].color = sf::Color::White;
		topBorderArray[0].position.x = bounds.left;
		topBorderArray[0].position.y = bounds.top + bounds.height;
		topBorderArray[1].color = sf::Color::White;
		topBorderArray[1].position.x = bounds.left;
		topBorderArray[1].position.y = bounds.top;
		topBorderArray[2].color = sf::Color::White;
		topBorderArray[2].position.x = bounds.left + bounds.width;
		topBorderArray[2].position.y = bounds.top;
		mVertices.push_back(topBorderArray);
		mVertices.push_back(topBorderArray);
		mVertices.push_back(topBorderArray);
		for (std::size_t i = 0; i < mVertices[6].getVertexCount(); ++i) {
			mVertices[7][i].color = sf::Color(127, 127, 127, 255);
			mVertices[8][i].color = sf::Color::Black;
		}

		//Bottom border
		sf::VertexArray botBorderArray;
		botBorderArray.setPrimitiveType(sf::LinesStrip);
		botBorderArray.resize(3);
		botBorderArray[0].color = sf::Color::White;
		botBorderArray[0].position.x = bounds.left;
		botBorderArray[0].position.y = bounds.top + bounds.height;
		botBorderArray[1].color = sf::Color::White;
		botBorderArray[1].position.x = bounds.left + bounds.width;
		botBorderArray[1].position.y = bounds.top + bounds.height;
		botBorderArray[2].color = sf::Color::White;
		botBorderArray[2].position.x = bounds.left + bounds.width;
		botBorderArray[2].position.y = bounds.top;
		mVertices.push_back(botBorderArray);
		mVertices.push_back(botBorderArray);
		mVertices.push_back(botBorderArray);
		for (std::size_t i = 0; i < mVertices[9].getVertexCount(); ++i) {
			mVertices[10][i].color = sf::Color(127, 127, 127, 255);
			mVertices[11][i].color = sf::Color::Black;
		}
	}
}

void MenuItem::setDisabled(bool status)
{
	mDisabled = status;
}

bool MenuItem::isDisabled() const
{
	return mDisabled;
}

void MenuItem::handleEvents(sf::Event const & ev)
{
	if (mForcedState) {
		return;
	}
	switch (ev.type) {
		//first pass gets mouse position
	case sf::Event::MouseMoved:
		if (this->getTransform().transformRect(mBounds).contains((float)ev.mouseMove.x, (float)ev.mouseMove.y)) {
			//if mouse is over button, set hovered flag
			mState |= MIS_HOVERED;
		}
		else {
			mState &= ~MIS_HOVERED;
		}
		break;
		//second pass gets mouse pressed
	case sf::Event::MouseButtonPressed:
		if (ev.mouseButton.button == sf::Mouse::Left) {
			if (mState == MIS_HOVERED) {
				//Sets pressed flag if button is being hovered
				mState |= MIS_PRESSED;
			}
		}
		break;
	case sf::Event::MouseButtonReleased:
		if (ev.mouseButton.button == sf::Mouse::Left) {
			if (mState == (MIS_HOVERED | MIS_PRESSED))
			{
				//Calls callback for button if both state flags are set and the mouse has been let go
				for (std::size_t i = 0; i < _callbacks.size(); i++) {
					_callbacks[i](*this);
				}
			}
		}
		//resets state flags
		mState &= ~MIS_PRESSED;

		break;
	}
}


//Step 4b Inmplement the method to register a callback
void MenuItem::registerCallback(const cb_t &callback)
{
	_callbacks.push_back(callback);

}
void MenuItem::unregisterCallbacks()
{
	//Code which didn't work...
	//for (size_t i = 0; i < _callbacks.size(); i++) {
	//}
	//for (std::vector<cb_t>::iterator i = _callbacks.begin(); i != _callbacks.end(); i++) {

	//	_callbacks.erase(i);
	//	if (_callbacks.empty()) {
	//		break;
	//	}
	//}
	_callbacks.clear();
}


void MenuItem::setForcedState(bool force, int forcedState)
{
	mForced = force;
	mState = forcedState;
}
float MenuItem::getWidth()
{
	return mBounds.width;
}
float MenuItem::getHeight()
{
	return mBounds.height;
}
void MenuItem::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	//Remember that the vertices go White, Grey, Black
	// and the order goes Text, Underline, Top/Left Border, Bottom/Right Border
	enum MI_Color { WHITE, GREY, BLACK, MI_COLOR_COUNT };
	enum MI_Components { TEXT, UNDERLINE = MI_COLOR_COUNT, TL_BORDER = MI_COLOR_COUNT * 2, BR_BORDER = MI_COLOR_COUNT * 3 };
	states.transform *= this->getTransform();
	if (mDisabled) {
		target.draw(mVertices[TEXT + GREY], states);
		target.draw(mVertices[TL_BORDER + GREY], states);
		target.draw(mVertices[BR_BORDER + GREY], states);
	}
	else if (mState == MIS_NORMAL) {
		target.draw(mVertices[TEXT + WHITE], states);
		target.draw(mVertices[TL_BORDER + WHITE], states);
		target.draw(mVertices[BR_BORDER + BLACK], states);
	}
	else if (mState == MIS_HOVERED) {
		target.draw(mVertices[TEXT + WHITE], states);
		target.draw(mVertices[UNDERLINE + WHITE], states);
		target.draw(mVertices[TL_BORDER + WHITE], states);
		target.draw(mVertices[BR_BORDER + BLACK], states);
	}
	else if (mState == MIS_PRESSED) {
		target.draw(mVertices[TEXT + GREY], states);
		target.draw(mVertices[TL_BORDER + WHITE], states);
		target.draw(mVertices[BR_BORDER + BLACK], states);
	}
	else if (mState == (MIS_PRESSED | MIS_HOVERED)) {
		target.draw(mVertices[TEXT + GREY], states);
		target.draw(mVertices[UNDERLINE + GREY], states);
		target.draw(mVertices[TL_BORDER + BLACK], states);
		target.draw(mVertices[BR_BORDER + WHITE], states);
	}

}

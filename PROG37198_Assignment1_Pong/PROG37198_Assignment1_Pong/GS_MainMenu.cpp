#include "GS_MainMenu.h"
#include "AssetManager.h"

#include <SFML/Graphics.hpp>
#include <iostream>
#include "GameStateManager.h"
#include "GS_Options.h"
#include "GS_Pong.h"

GS_MainMenu::GS_MainMenu()
{
}


GS_MainMenu::~GS_MainMenu()
{
}


void GS_MainMenu::init()
{
	//Create Menu Text
	_menuText.emplace_back(AssetManager::getInstance().getText("PONG", 60), sf::Transform());
	_menuText.back().second.translate(SCREEN_WIDTH/2 - _menuText.back().first.getBounds().width/2, 15.0f);
	
	//Create Menu Buttons
	_menuButtons.emplace_back("Play", 10.0f, sf::Color::Cyan);
	_menuButtons.back().setPosition(SCREEN_WIDTH / 2 - _menuButtons.back().getWidth()/2, 100.0f);
	//Register callback using lambda (because it seems wasteful and messy to have a private function declared in the header, defined in the cpp, and bound with std::bound... This saves so much time)
	_menuButtons.back().registerCallback([](MenuItem &caller) 
	{
		AssetManager::getInstance().playSound(SoundEffect::BLIP);
		GameStateManager::getInstance().push(GS_Pong::getInstance());
	});
	_menuButtons.emplace_back("Options", 10.0f, sf::Color::Cyan);
	_menuButtons.back().setPosition(SCREEN_WIDTH / 2 - _menuButtons.back().getWidth()/2, 150.0f);
	_menuButtons.back().registerCallback([](MenuItem &caller)
	{
		AssetManager::getInstance().playSound(SoundEffect::BLIP);
		GameStateManager::getInstance().push(GS_Options::getInstance());
	});
	_menuButtons.emplace_back("Exit", 10.0f, sf::Color::Cyan);
	_menuButtons.back().setPosition(SCREEN_WIDTH / 2 - _menuButtons.back().getWidth()/2, 200.0f);
	_menuButtons.back().registerCallback([](MenuItem &caller)
	{
		AssetManager::getInstance().playSound(SoundEffect::BLIP);
		GameStateManager::getInstance().pop();
	});

}

void GS_MainMenu::deinit()
{
	//unregister callbacks
	for (std::size_t i = 0; i < _menuButtons.size(); i++) {
		_menuButtons[i].unregisterCallbacks();
	}
	_menuButtons.clear();
	//Destroy menu objects
	_menuText.clear();

}

void GS_MainMenu::pause()
{
}

void GS_MainMenu::resume()
{
}

bool GS_MainMenu::handleEvent(sf::Event const & ev)
{
	for (std::size_t i = 0; i < _menuButtons.size(); i++) {
		_menuButtons[i].handleEvents(ev);
	}
	return false;
}

void GS_MainMenu::update(float delta)
{

}

void GS_MainMenu::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.clear(sf::Color(100, 100, 100));
	for (std::size_t i = 0; i < _menuText.size(); ++i) {
		target.draw(_menuText[i].first, _menuText[i].second);
	}
	for (std::size_t i = 0; i < _menuButtons.size(); i++) {
		target.draw(_menuButtons[i]);
	}
}

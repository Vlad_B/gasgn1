#include "GS_Instructions.h"

#include <SFML/Graphics.hpp>
#include "AssetManager.h"

GS_Instructions::GS_Instructions() : AGameState(false)
, _background(sf::Vector2f((float)SCREEN_WIDTH * 0.5f, (float)SCREEN_HEIGHT * 0.8f))
{
	_background.setFillColor(sf::Color(100, 100, 100));
	_background.setPosition((float)SCREEN_WIDTH * 0.25f, (float)SCREEN_HEIGHT * 0.1f);
}

GS_Instructions::~GS_Instructions()
{
}

void GS_Instructions::init()
{
	_menuText.emplace_back(AssetManager::getInstance().getText("How to play", 20), sf::Transform());
	_menuText.back().second.translate(SCREEN_WIDTH / 2 - _menuText.back().first.getBounds().width / 2, SCREEN_HEIGHT * 0.15f);

	_menuText.emplace_back(AssetManager::getInstance().getText("W/S          P1 Paddle", 15), sf::Transform());
	_menuText.back().second.translate(SCREEN_WIDTH / 2 - _menuText.back().first.getBounds().width / 2, SCREEN_HEIGHT * 0.15f +30);

	_menuText.emplace_back(AssetManager::getInstance().getText("Up/Down      P2 Paddle", 15), sf::Transform());
	_menuText.back().second.translate(SCREEN_WIDTH / 2 - _menuText.back().first.getBounds().width / 2, SCREEN_HEIGHT * 0.15f + 60);

	_menuText.emplace_back(AssetManager::getInstance().getText("I            Toggle AI", 15), sf::Transform());
	_menuText.back().second.translate(SCREEN_WIDTH / 2 - _menuText.back().first.getBounds().width / 2, SCREEN_HEIGHT * 0.15f + 90);

	_menuText.emplace_back(AssetManager::getInstance().getText("AI Takes over player 2", 10), sf::Transform());
	_menuText.back().second.translate(SCREEN_WIDTH / 2 - _menuText.back().first.getBounds().width / 2, SCREEN_HEIGHT * 0.15f + 120);

	_menuText.emplace_back(AssetManager::getInstance().getText("Rx2            Restart", 15), sf::Transform());
	_menuText.back().second.translate(SCREEN_WIDTH / 2 - _menuText.back().first.getBounds().width / 2, SCREEN_HEIGHT * 0.15f + 150);

	_menuText.emplace_back(AssetManager::getInstance().getText("Try to perform a smash", 15), sf::Transform());
	_menuText.back().second.translate(SCREEN_WIDTH / 2 - _menuText.back().first.getBounds().width / 2, SCREEN_HEIGHT * 0.15f + 210);

	_menuText.emplace_back(AssetManager::getInstance().getText("attack by striking the", 15), sf::Transform());
	_menuText.back().second.translate(SCREEN_WIDTH / 2 - _menuText.back().first.getBounds().width / 2, SCREEN_HEIGHT * 0.15f + 240);

	_menuText.emplace_back(AssetManager::getInstance().getText("ball with your corner!", 15), sf::Transform());
	_menuText.back().second.translate(SCREEN_WIDTH / 2 - _menuText.back().first.getBounds().width / 2, SCREEN_HEIGHT * 0.15f + 270);
	
	_menuButtons.emplace_back("Back", 10.0f, sf::Color::Cyan);
	_menuButtons.back().setPosition(SCREEN_WIDTH / 2 - _menuButtons.back().getWidth() / 2, SCREEN_HEIGHT - (SCREEN_HEIGHT * 0.15f) - 20.0f);
	_menuButtons.back().registerCallback([](MenuItem &caller)
	{
		AssetManager::getInstance().playSound(SoundEffect::BLIP);
		GameStateManager::getInstance().pop();
	});
}

void GS_Instructions::deinit()
{
}

void GS_Instructions::pause()
{
}

void GS_Instructions::resume()
{
}

bool GS_Instructions::handleEvent(sf::Event const &ev)
{
	for (size_t i = 0; i < _menuButtons.size(); i++) {
		_menuButtons[i].handleEvents(ev);
	}
	return false;
}

void GS_Instructions::update(float delta)
{
	
}

void GS_Instructions::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(_background);
	for (size_t i = 0; i < _menuButtons.size(); i++) {
		target.draw(_menuButtons[i]);
	}
	for (size_t i = 0; i < _menuText.size(); i++) {
		target.draw(_menuText[i].first, _menuText[i].second);
	}
}
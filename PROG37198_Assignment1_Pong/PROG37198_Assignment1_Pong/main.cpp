#include "GameStateManager.h"
#include "GS_Splash.h"
#include "GS_Options.h"
#include "GS_Pong.h"
#include "AssetManager.h"

#include <string>
#include <iostream>

int main(int argc, char *argv[]) {
	std::cout << "Assignment 1!" << std::endl;//Excitement about assignment 1
	//Getting gamestate manager reference (not sure why since it's a singleton)
	GameStateManager &gs = GameStateManager::getInstance();
	//getting splash state reference (also not sure why since it's also a singleton)
	GS_Splash &splash = GS_Splash::getInstance();
	gs.push(GS_Options::getInstance());//DIRTY FILTHY HACK!
	gs.swap(GS_Splash::getInstance());
	//TEST GAME
	//GS_Pong &pong = GS_Pong::getInstance();
	//gs.push(pong);
	int retval = gs.play();

	std::cout << "Press enter key to quit" << std::endl;
	std::string str;
	std::getline(std::cin, str);
	return retval;
}
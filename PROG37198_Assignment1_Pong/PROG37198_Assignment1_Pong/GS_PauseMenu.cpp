#include "GS_PauseMenu.h"
#include "AssetManager.h"

#include <SFML/Graphics.hpp>
#include <iostream>
#include "GameStateManager.h"
#include "GS_MainMenu.h"
#include "GS_Options.h"
#include "GS_Instructions.h"

GS_PauseMenu::GS_PauseMenu() 
	: AGameState(false)
	, mBackground(sf::Vector2f((float)SCREEN_WIDTH * 0.5f, (float)SCREEN_HEIGHT * 0.8f))
{
	mBackground.setFillColor(sf::Color(100, 100, 100));
	mBackground.setPosition((float)SCREEN_WIDTH * 0.25f, (float)SCREEN_HEIGHT * 0.1f);
}


GS_PauseMenu::~GS_PauseMenu()
{
}



void GS_PauseMenu::init()
{
	_pauseText.emplace_back(AssetManager::getInstance().getText("PAUSED", 60), sf::Transform());
	_pauseText.back().second.translate(SCREEN_WIDTH / 2 - _pauseText.back().first.getBounds().width / 2, SCREEN_HEIGHT * 0.15f);

	_pauseButtons.emplace_back("Return to Main Menu", 10.0f, sf::Color::Cyan);
	_pauseButtons.back().setPosition(SCREEN_WIDTH / 2 - _pauseButtons.back().getWidth() / 2, (SCREEN_HEIGHT * 0.15f) + 100.0f);
	_pauseButtons.back().registerCallback([](MenuItem &caller)
	{
		AssetManager::getInstance().playSound(SoundEffect::BLIP);
		GameStateManager::getInstance().popAllThenPush(GS_MainMenu::getInstance());
	});

	_pauseButtons.emplace_back("How To Play", 10.0f, sf::Color::Cyan);
	_pauseButtons.back().setPosition(SCREEN_WIDTH / 2 - _pauseButtons.back().getWidth() / 2, (SCREEN_HEIGHT * 0.15f) + 150.0f);
	_pauseButtons.back().registerCallback([](MenuItem &caller)
	{
		AssetManager::getInstance().playSound(SoundEffect::BLIP);
		GameStateManager::getInstance().push(GS_Instructions::getInstance());
	});

	_pauseButtons.emplace_back("Sound", 10.0f, sf::Color::Cyan);
	_pauseButtons.back().setPosition(SCREEN_WIDTH / 2 - _pauseButtons.back().getWidth() / 2, (SCREEN_HEIGHT * 0.15f) + 200.0f);
	_pauseButtons.back().registerCallback([](MenuItem &caller)
	{
		AssetManager::getInstance().playSound(SoundEffect::BLIP);
		GameStateManager::getInstance().push(GS_Options::getInstance());
	});

	_pauseButtons.emplace_back("Resume", 10.0f, sf::Color::Cyan);
	_pauseButtons.back().setPosition(SCREEN_WIDTH / 2 - _pauseButtons.back().getWidth() / 2, SCREEN_HEIGHT - (SCREEN_HEIGHT * 0.15f) - 20.0f);
	_pauseButtons.back().registerCallback([](MenuItem &caller)
	{
		AssetManager::getInstance().playSound(SoundEffect::BLIP);
		GameStateManager::getInstance().pop();
	});
}

void GS_PauseMenu::deinit()
{
	//unregister callbacks
	for (std::size_t i = 0; i < _pauseButtons.size(); i++) {
		_pauseButtons[i].unregisterCallbacks();
	}
	//Destroy menu objects
	_pauseText.clear();
	_pauseButtons.clear();
}

void GS_PauseMenu::pause()
{
}

void GS_PauseMenu::resume()
{
}

bool GS_PauseMenu::handleEvent(sf::Event const &ev)
{
	for (std::size_t i = 0; i < _pauseButtons.size(); i++) {
		_pauseButtons[i].handleEvents(ev);
	}
	if (ev.type == sf::Event::KeyReleased && ev.key.code == sf::Keyboard::Escape) {
		GameStateManager::getInstance().pop();
	}
	return false;
}

void GS_PauseMenu::update(float delta)
{

}

void GS_PauseMenu::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(mBackground);
	for (std::size_t i = 0; i < _pauseText.size(); ++i) {
		target.draw(_pauseText[i].first, _pauseText[i].second);
	}
	for (std::size_t i = 0; i < _pauseButtons.size(); i++) {
		target.draw(_pauseButtons[i]);
	}
}
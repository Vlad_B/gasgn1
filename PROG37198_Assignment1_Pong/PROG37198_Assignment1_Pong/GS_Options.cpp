#include "GS_Options.h"
#include "AssetManager.h"
#include "GameStateManager.h"

#include <SFML/Graphics.hpp>
#include <iostream>

GS_Options::GS_Options() : AGameState(false)
{
}


GS_Options::~GS_Options()
{
}



void GS_Options::init()
{
	//Starts by clearing the vectors (not really necessery, but there to try to address a bug)
	_optButtons.clear();
	_optSliders.clear();
	_optText.clear();

	//Label
	_optText.emplace_back(AssetManager::getInstance().getText("Sound", 60), sf::Transform());
	_optText.back().second.translate(SCREEN_WIDTH / 2 - _optText.back().first.getBounds().width / 2, 15.0f);

	//Button to toggle first song
	_optButtons.emplace_back("Song 1", 15.0f, sf::Color::Cyan);
	_optButtons.back().setPosition(SCREEN_WIDTH / 3 - _optButtons.back().getWidth() / 2, 100.0f);
	_optButtons.back().registerCallback([](MenuItem &caller)
	{
		AssetManager::getInstance().playSound(SoundEffect::BLIP);
		//Song toggling logic
		//if flag for first song is set, then call stop, otherwise call start
		if ((AssetManager::getInstance().getCurrentlyPlayingFlags() & PlayingMusic::SONG1) == PlayingMusic::SONG1) {
			AssetManager::getInstance().stopMusic(PlayingMusic::SONG1);
		}
		else {
			AssetManager::getInstance().startMusic(PlayingMusic::SONG1);
		}
	});

	//Button for second song (Same logic as with first)
	_optButtons.emplace_back("Song 2", 15.0f, sf::Color::Cyan);
	_optButtons.back().setPosition(SCREEN_WIDTH / 3 + _optButtons.back().getWidth()*2.0f, 100.0f);
	_optButtons.back().registerCallback([](MenuItem &caller)
	{
		AssetManager::getInstance().playSound(SoundEffect::BLIP);
		if ((AssetManager::getInstance().getCurrentlyPlayingFlags() & PlayingMusic::SONG2) == PlayingMusic::SONG2) {
			AssetManager::getInstance().stopMusic(PlayingMusic::SONG2);
		}
		else {
			AssetManager::getInstance().startMusic(PlayingMusic::SONG2);
		}
	});

	//Label for music volume slider
	_optText.emplace_back(AssetManager::getInstance().getText("Music", 15), sf::Transform());
	_optText.back().second.translate(SCREEN_WIDTH / 2 - _optText.back().first.getBounds().width / 2,195);

	//music volume slider
	_optSliders.emplace_back(300, 0, sf::Color::Cyan);
	_optSliders.back().setPosition(SCREEN_WIDTH / 2 - _optSliders.back().getWidth() / 2, 200.0f);
	
	//Callbacks for slider
	_optSliders.back().registerLessCallback(std::bind(&GS_Options::_musicDownBindMethod, this, std::placeholders::_1));
	_optSliders.back().registerMoreCallback(std::bind(&GS_Options::_musicUpBindMethod, this, std::placeholders::_1));

	//label for sfx volume slider
	_optText.emplace_back(AssetManager::getInstance().getText("SFX", 15), sf::Transform());
	_optText.back().second.translate(SCREEN_WIDTH / 2 - _optText.back().first.getBounds().width / 2, 295);
	
	//sfx volume slider
	_optSliders.emplace_back(300, 0, sf::Color::Cyan);
	_optSliders.back().setPosition(SCREEN_WIDTH / 2 - _optSliders.back().getWidth() / 2, 300.0f);
	
	//callbacks
	_optSliders.back().registerLessCallback(std::bind(&GS_Options::_sfxDownBindMethod, this, std::placeholders::_1));
	_optSliders.back().registerMoreCallback(std::bind(&GS_Options::_sfxUpBindMethod, this, std::placeholders::_1));

	//back button
	_optButtons.emplace_back("Back", 10.0f, sf::Color::Cyan);
	_optButtons.back().setPosition(SCREEN_WIDTH / 2 - _optButtons.back().getWidth() / 2, 400.0f);
	_optButtons.back().registerCallback([](MenuItem &caller)
	{
		AssetManager::getInstance().playSound(SoundEffect::BLIP);
		GameStateManager::getInstance().pop();
	});
}

void GS_Options::deinit()
{
	//unregister callbacks
	for (std::size_t i = 0; i < _optButtons.size(); i++) {
		_optButtons[i].unregisterCallbacks();
	}
	_optButtons.clear();
	//unregister slider callbacks
	for (std::size_t i = 0; i < _optSliders.size(); i++) {
		_optSliders[i].unregisterCallbacks();
	}
	_optSliders.clear();
	//Destroy menu objects
	_optText.clear();
}

void GS_Options::pause()
{
}

void GS_Options::resume()
{
}

bool GS_Options::handleEvent(sf::Event const &ev)
{
	for (std::size_t i = 0; i < _optButtons.size(); i++) {
		_optButtons[i].handleEvents(ev);
	}
	for (std::size_t i = 0; i < _optSliders.size(); i++) {
		_optSliders[i].handleEvents(ev);
	}
	return false;
}

//attempt at making the buttons for the playing music to be indented in order to show better if a music piece is playing, but ultimately failed (kind of)
void GS_Options::update(float delta)
{
	//if ((AssetManager::getInstance().getCurrentlyPlayingFlags() & PlayingMusic::SONG1) == PlayingMusic::SONG1) {
	//	_optButtons[0].setForcedState(false, MIS_PRESSED | MIS_HOVERED);
	//}
	//if ((AssetManager::getInstance().getCurrentlyPlayingFlags() & PlayingMusic::SONG2) == PlayingMusic::SONG2) {
	//	_optButtons[1].setForcedState(false, MIS_PRESSED | MIS_HOVERED);
	//}
}


void GS_Options::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.clear(sf::Color(100, 100, 100));
	for (std::size_t i = 0; i < _optText.size(); ++i) {
		target.draw(_optText[i].first, _optText[i].second);
	}
	for (std::size_t i = 0; i < _optButtons.size(); i++) {
		target.draw(_optButtons[i]);
	}
	for (std::size_t i = 0; i < _optSliders.size(); i++) {
		target.draw(_optSliders[i]);
	}
}

#pragma region Binding Methods



void GS_Options::_musicUpBindMethod(MenuItem &caller)
{
	AssetManager::getInstance().playSound(SoundEffect::BLIP);
	std::cout << _optSliders[0].getSliderValue();
	AssetManager::getInstance().setMusicVolume(SOUNDMAXVOLUME * _optSliders[0].getSliderValue());
}

void GS_Options::_musicDownBindMethod(MenuItem &caller)
{
	AssetManager::getInstance().playSound(SoundEffect::BLIP);
	std::cout << _optSliders[0].getSliderValue();
	AssetManager::getInstance().setMusicVolume(SOUNDMAXVOLUME * _optSliders[0].getSliderValue());
}

void GS_Options::_sfxUpBindMethod(MenuItem &caller)
{
	AssetManager::getInstance().playSound(SoundEffect::BLIP);
	std::cout << _optSliders[1].getSliderValue();
	AssetManager::getInstance().setSfxVolume(SOUNDMAXVOLUME * _optSliders[1].getSliderValue());
}

void GS_Options::_sfxDownBindMethod(MenuItem &caller)
{
	AssetManager::getInstance().playSound(SoundEffect::BLIP);
	std::cout << _optSliders[1].getSliderValue();
	AssetManager::getInstance().setSfxVolume(SOUNDMAXVOLUME * _optSliders[1].getSliderValue());
}

#pragma endregion
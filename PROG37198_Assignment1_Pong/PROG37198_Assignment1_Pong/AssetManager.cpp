#include "AssetManager.h"

#include <cctype>
#include <iostream>


AssetManager::AssetManager()
{
	//Characters
	// Uppercase
	mChars['A'] = { {0,1}, {0,4},	{0,1}, {1,0},	{1,0}, {2,1},	{2,1}, {2,4},	{0,2}, {2,2} };
	mChars['B'] = { {0,0}, {0,4},	{0,0}, {1,0},	{1,0}, {2,1},	{2,1}, {2,2},	{2,2}, {2,3},	{2,3}, {1,4},	{0,2}, {2,2},	{0,4}, {1,4} };
	mChars['C'] = { {0,1}, {0,3},	{0,1}, {1,0},	{1,0}, {2,0},	{1,4}, {2,4},	{0,3}, {1,4} };
	mChars['D'] = { {0,0}, {0,4},	{0,0}, {1,0},	{1,0}, {2,1},	{2,1}, {2,2},	{2,2}, {2,3},	{2,3}, {1,4},	{0,4}, {1,4} };
	mChars['E'] = { {0,0}, {0,4},	{0,0}, {2,0},	{0,2}, {1,2},	{0,4}, {2,4} };
	mChars['F'] = { {0,0}, {0,4},	{0,0}, {2,0},	{0,2}, {1,2} };
	mChars['G'] = { {0,1}, {0,3},	{0,1}, {1,0},	{1,0}, {2,0},	{1,4}, {2,4},	{0,3}, {1,4},	{2,4}, {2,2},	{2,2}, {1,2} };
	mChars['H'] = { {0,0}, {0,4},	{2,0}, {2,4},	{0,2}, {2,2} };
	mChars['I'] = { {0,0}, {2,0},	{1,0}, {1,4},	{0,4}, {2,4} };
	mChars['J'] = { {0,0}, {2,0},	{2,0}, {2,4},	{1,4}, {2,4},	{1,4}, {0,3} };
	mChars['K'] = { {0,0}, {0,4},	{0,2}, {2,4},	{0,2}, {2,0} };
	mChars['L'] = { {0,0}, {0,4},	{0,4}, {2,4} };
	mChars['M'] = { {0,0}, {0,4},	{0,0}, {1,2},	{1,2}, {2,0},	{2,0}, {2,4} };
	mChars['N'] = { {0,0}, {0,4},	{0,0}, {2,4},	{2,0}, {2,4} };
	mChars['O'] = { {0,1}, {0,3},	{0,1}, {1,0},	{1,0}, {2,1},	{2,1}, {2,3},	{2,3}, {1,4},	{1,4}, {0,3} };
	mChars['P'] = { {0,0}, {0,4},	{0,0}, {1,0},	{1,0}, {2,1},	{2,1}, {2,2},	{0,2}, {2,2} };
	mChars['Q'] = { {0,1}, {0,3},	{0,1}, {1,0},	{1,0}, {2,1},	{2,1}, {2,3},	{2,3}, {1,4},	{1,4}, {0,3},	{1,3}, {2,4} };
	mChars['R'] = { {0,0}, {0,4},	{0,0}, {1,0},	{1,0}, {2,1},	{2,1}, {2,2},	{0,2}, {2,2},	{0,2}, {2,4} };
	mChars['S'] = { {1,0}, {2,0},	{1,0}, {0,1},	{0,1}, {0,2},	{0,2}, {2,2},	{2,2}, {2,3},	{2,3}, {1,4},	{1,4}, {0,4} };
	mChars['T'] = { {0,0}, {2,0},	{1,0}, {1,4} };
	mChars['U'] = { {0,0}, {0,3},	{2,0}, {2,3},	{0,3}, {1,4},	{1,4}, {2,3} };
	mChars['V'] = { {0,0}, {1,4},	{1,4}, {2,0} };
	mChars['W'] = { {0,0}, {0,4},	{0,4}, {1,2},	{1,2}, {2,4},	{2,0}, {2,4} };
	mChars['X'] = { {0,0}, {2,4},	{0,4}, {2,0} };
	mChars['Y'] = { {0,0}, {1,2},	{1,2}, {2,0},	{1,2}, {1,4} };
	mChars['Z'] = { {0,0}, {2,0},	{2,0}, {0,4},	{0,4}, {2,4} };

	//Numbers
	mChars['0'] = { {0,1}, {0,3},	{0,1}, {1,0},	{1,0}, {2,1},	{2,1}, {2,3},	{2,3}, {1,4},	{1,4}, {0,3},	{0,3}, {2,1} };
	mChars['1'] = { {0,1}, {1,0},	{1,0}, {1,4},	{0,4}, {2,4} };
	mChars['2'] = { {0,0}, {2,0},	{2,0}, {2,1},	{2,1}, {0,4},	{0,4}, {2,4} };
	mChars['3'] = { {0,0}, {1,0},	{1,0}, {2,1},	{2,1}, {2,3},	{0,4}, {1,4},	{1,4}, {2,3},	{1,2}, {2,2} };
	mChars['4'] = { {0,0}, {0,2},	{2,0}, {2,4},	{0,2}, {2,2} };
	mChars['5'] = { {0,0}, {2,0},	{0,0}, {0,2},	{0,2}, {2,2},	{2,2}, {2,4},	{2,4}, {0,4} };
	mChars['6'] = { {0,0}, {0,4},	{0,0}, {2,0},	{0,4}, {2,4},	{2,4}, {2,2},	{2,2}, {0,2} };
	mChars['7'] = { {0,0}, {2,0},	{2,0}, {0,4} };
	mChars['8'] = { {0,0}, {0,4},	{0,0}, {2,0},	{2,0}, {2,4},	{0,2}, {2,2},	{0,4}, {2,4} };
	mChars['9'] = { {2,0}, {2,4},	{0,0}, {2,0},	{0,0}, {0,1},	{0,1}, {1,2},	{1,2}, {2,2} };

	//Punctuation
	mChars[' '] = {};
	mChars['.'] = { {0,3}, {1,3},	{1,3}, {1,4},	{1,4}, {0,4},	{0,4}, {0,3} };
	mChars[','] = { {0,3}, {1,3},	{1,3}, {1,4},	{1,4}, {0,3} };
	mChars['&'] = { {2,4}, {0,1},	{0,1}, {1,0},	{1,0}, {2,1},	{2,1}, /*{0,2},	{0,2},*/ {0,3},	{0,3}, {1,4},	{1,4}, {2,2} };

	//Use bell for all unknown characters. For best effect offset by +0.5,+0.5
	mChars[(char)7] = { {0,0}, {0,3},	{0,3}, {1,3},	{1,3}, {1,0},	{1,0}, {0,0},	{0,1}, {1,1} };

	//Sprites
	//Origin is from top center pixel of ships
	mSprites["needle_classic"] = { {0,0}, {0,4},	{0,4}, {1,5},	{1,5}, {1,30},		{1,22}, {3,24},			{3,24}, {3,30},
													{0,4}, {-1,5},	{-1,5}, {-1,30},	{-1,22}, {-3,24},		{-3,24}, {-3,30},
								   {-3,30}, {3,30} };
	mSprites["wedge_classic"] = { {0,0}, {0,2},	{0,2}, {1,3},		{1,3}, {1,5},		{1,5}, {2,6},		{2,6}, {2,8},		{2,8}, {3,9},		{3,9}, {3,16},			{3,14}, {4,15},			{4,15}, {4,16},			{4,16}, {5,17},			{5,17},{5,18},		{5,18}, {6,19},		{6,19}, {6,24},
													{0,2}, {-1,3},		{-1,3}, {-1,5},		{-1,5}, {-2,6},		{-2,6}, {-2,8},		{-2,8}, {-3,9},		{-3,9}, {-3,16},		{-3,14}, {-4,15},		{-4,15}, {-4,16},		{-4,16}, {-5,17},		{-5,17},{-5,18},	{-5,18}, {-6,19},	{-6,19}, {-6,24},
													{3,16}, {2,17},		{2,17}, {2,21},		{2,21}, {5,24},		{5,24}, {6,24},		{2,21}, {1,22},		{1,22}, {1,24},
													{-3,16}, {-2,17},	{-2,17}, {-2,21},	{-2,21}, {-5,24},	{-5,24}, {-6,24},	{-2,21}, {-1,22},	{-1,22}, {-1,24},
													{-1,24}, {1,24} };

	//Origin is top-left corner of paddle
	mSprites["paddle"] = { { 0,0 },{ 10, 0 },{ 10,60 },{ 0, 60 } };
	mSprites["ball"] = { { 0,0 },{ 5, 0 },{ 5,5 },{ 0, 5 } };

#pragma region Sound Initialization

	//Start with all music playing
	_playingMusic = PlayingMusic::ALL;

	//Adding blip sfx
	sf::SoundBuffer *buffer = new sf::SoundBuffer();
	buffer->loadFromFile("blip.wav");
	_soundBuffers.push_back(buffer);
	_sounds.push_back(sf::Sound(*_soundBuffers.back()));

	//Adding jump sfx
	buffer = new sf::SoundBuffer();
	buffer->loadFromFile("jump.wav");
	_soundBuffers.push_back(buffer);
	_sounds.push_back(sf::Sound(*_soundBuffers.back()));

	//Adding ring sfx
	buffer = new sf::SoundBuffer();
	buffer->loadFromFile("Ring05.wav");
	_soundBuffers.push_back(buffer);
	_sounds.push_back(sf::Sound(*_soundBuffers.back()));

	//making stray buffer point to nullptr(because the locations to the buffers are located in the sound buffers vector
	buffer = nullptr;

	///Adding music to Vector
	_music.push_back(new sf::Music());
	_music.back()->openFromFile("johnpeel_1.ogg");
	_music.push_back(new sf::Music());
	_music.back()->openFromFile("johnpeel_2.ogg");

	//Set Volumes
	setMusicVolume(SOUNDSTARTVOLUME);
	setSfxVolume(SOUNDSTARTVOLUME);

	//Start music
	_music[0]->play();
	_music[1]->play();


#pragma endregion

}

//Clean up the music and buffer pointers from memory so they don't leak
AssetManager::~AssetManager()
{
	//Deletes all music pointers to avoid leakage
	for (std::size_t i = 0; i < _music.size(); ++i) {
		delete _music[i];
		_music[i] = nullptr;
	}
	_music.clear();

	//Deletes all sound buffers pointers to avoid leakage
	for (std::size_t i = 0; i < _sounds.size(); ++i) {
		delete _soundBuffers[i];
		_soundBuffers[i] = nullptr;
	}
	_soundBuffers.clear();
	_sounds.clear();
}



sf::VertexArray AssetManager::getSprite(std::string const & name)
{
	//Setup the vertex array
	sf::VertexArray vertices;
	vertices.setPrimitiveType(sf::Lines);
	auto it = mSprites.find(name);
	if (it != mSprites.end()) {
		std::size_t vcount = vertices.getVertexCount();
		vertices.resize(vertices.getVertexCount() + it->second.size());
		for (std::size_t j = 0; j < it->second.size(); ++j) {
			vertices[vcount + j].color = sf::Color::White;
			vertices[vcount + j].position.x = (float)it->second[j].x;
			vertices[vcount + j].position.y = (float)it->second[j].y;
		}
	}
	return vertices;
}

sf::VertexArray AssetManager::getText(std::string const & istr, float cheight)
{

	//Setup the character offset and sizing values.
	float const CHARACTER_WIDTH = 3.0f;
	float const CHARACTER_X_SPACING = 1.0f; //Fixed for all character heights
	float const CHARACTER_HEIGHT = 4.0f;
	float const INVALID_CHARACTER_OFFSET = 0.5f;
	float currentXOffset = 0.0f;
	float yScale = cheight / CHARACTER_HEIGHT;
	float xScale = yScale;

	//Set the string to all uppercase letters
	std::string str = istr;
	for (std::size_t i = 0; i < str.length(); ++i) {
		if (std::isalpha((unsigned char)str[i])) {
			str[i] = std::toupper((unsigned char)str[i]);
		}
	}

	//Setup the vertex array
	sf::VertexArray vertices;
	vertices.setPrimitiveType(sf::Lines);
	for (std::size_t i = 0; i < str.length(); ++i) {
		//Search for the character in the string, if it exists copy
		// it into the vertices buffer, otherwise copy the 
		// unknown character symbol which is found in the spot for bell (0x07)
		auto it = mChars.find(str.at(i));
		if (it != mChars.end()) {
			std::size_t vcount = vertices.getVertexCount();
			vertices.resize(vcount + it->second.size());
			for (std::size_t j = 0; j < it->second.size(); ++j) {
				vertices[vcount + j].color = sf::Color::White;
				vertices[vcount + j].position.x = ((float)it->second[j].x * xScale) + currentXOffset;
				vertices[vcount + j].position.y = ((float)it->second[j].y) * yScale;
			}
			currentXOffset += (CHARACTER_WIDTH * xScale) + CHARACTER_X_SPACING;
		}
		else {
			it = mChars.find((char)7);
			std::size_t vcount = vertices.getVertexCount();
			vertices.resize(vcount + it->second.size());
			for (std::size_t j = 0; j < it->second.size(); ++j) {
				vertices[vcount + j].color = sf::Color::White;
				vertices[vcount + j].position.x = (((float)it->second[j].x + INVALID_CHARACTER_OFFSET) * xScale) + currentXOffset;
				vertices[vcount + j].position.y = ((float)it->second[j].y + INVALID_CHARACTER_OFFSET) * yScale;
			}
			currentXOffset += (CHARACTER_WIDTH * xScale) + CHARACTER_X_SPACING;
		}
	}
	return vertices;
}
#pragma region SoundFX and Music

//Returns the flags of the currently playing music
PlayingMusic AssetManager::getCurrentlyPlayingFlags()
{
	return static_cast<PlayingMusic>(_playingMusic);
}

void AssetManager::startMusic(PlayingMusic song)
{
	//ORs the currently playing music to add the songs being provided in the arguement
	_playingMusic |= song;
	//Starts song 1 if that was in the flag
	if ((_playingMusic & PlayingMusic::SONG1) == PlayingMusic::SONG1) {
		_music[0]->play();
		_music[0]->setPlayingOffset(getCurrentPlayMaxTime(_music));
		_music[1]->setPlayingOffset(getCurrentPlayMaxTime(_music));
	}
	//same as above but for song 2
	if ((_playingMusic & PlayingMusic::SONG2) == PlayingMusic::SONG2) {
		_music[1]->play();
		_music[0]->setPlayingOffset(getCurrentPlayMaxTime(_music));
		_music[1]->setPlayingOffset(getCurrentPlayMaxTime(_music));
	}
}

void AssetManager::stopMusic(PlayingMusic song)
{
	//ANDS the current playing songs with the songs provided in the arguements
	_playingMusic &= ~song;
	//Stops song 1
	if ((_playingMusic & PlayingMusic::SONG1) != PlayingMusic::SONG1) {
		_music[0]->pause();
	}
	//Stops song 2
	if ((_playingMusic & PlayingMusic::SONG2) != PlayingMusic::SONG2) {
		_music[1]->pause();
	}
}

//Plays a sound effect which is mapped to the SoundEffect Enum
void AssetManager::playSound(SoundEffect sound)
{
	_sounds[sound].play();
}

void AssetManager::stopAllSounds()
{
	for (auto &sound : _sounds) {
		sound.stop();
	}
}


void AssetManager::setSfxVolume(float value)
{
	//Check if volume is within legal parameters, and if it isn't, set it to the most appropriate paramater available
	if (value < 0) {
		_sfxVolume = 0;
	}
	if (value > SOUNDMAXVOLUME) {
		_sfxVolume = SOUNDMAXVOLUME;
	}
	else {
		_sfxVolume = value;
	}
	//Apply to all sound effects
	for (int i = 0; i < _sounds.size(); i++) {
		_sounds[i].setVolume(value);
	}
}

float AssetManager::getSfxVolume()
{
	return _sfxVolume;
}

//Samme as set sfx volume
void AssetManager::setMusicVolume(float value)
{
	std::cout << value << std::endl;
	if (value < 0) {

		_musicVolume = 0;
	}
	if (value > SOUNDMAXVOLUME) {
		_musicVolume = SOUNDMAXVOLUME;
	}
	else {
		_musicVolume = value;
	}
	for (int i = 0; i < _music.size(); i++) {
		_music[i]->setVolume(value);
	}
}

float AssetManager::getMusicVolume()
{
	return _musicVolume;
}

//Returns the first instance of a playing music piece, or a new sf::Time() variable
sf::Time AssetManager::getCurrentPlayTime(std::vector<sf::Music*>& music)
{
	for (std::size_t i = 0; i < music.size(); ++i) {
		if (music[i]->getStatus() == sf::SoundSource::Playing) {
			return music[i]->getPlayingOffset();
		}
	}
	return sf::Time();
}

//Returns the max instance of a playing music piece, or a new sf::Time() variable
sf::Time AssetManager::getCurrentPlayMaxTime(std::vector<sf::Music*>& music)
{
	sf::Time max;
	for (std::size_t i = 0; i < music.size(); ++i) {
		if (music[i]->getStatus() == sf::SoundSource::Playing && music[i]->getPlayingOffset() > max) {
			max = music[i]->getPlayingOffset();
		}
	}
	return max;
}


sf::Time AssetManager::getCurrentPlayingTime()
{
	return getCurrentPlayMaxTime(_music);
}
#pragma endregion
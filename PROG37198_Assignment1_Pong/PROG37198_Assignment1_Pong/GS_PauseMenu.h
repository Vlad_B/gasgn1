#pragma once
#include "AGameState.h"
#include <SFML/Graphics/RectangleShape.hpp>
#include <vector>
#include "MenuItem.h"
class GS_PauseMenu : public AGameState
{
	//Singleton-a-fy
	GS_SINGLETON(GS_PauseMenu)
public:
	// Inherited via AGameState
	virtual void init() override;
	virtual void deinit() override;
	virtual void pause() override;
	virtual void resume() override;
	virtual bool handleEvent(sf::Event const &) override;
	virtual void update(float delta) override;
protected:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const override;
	sf::RectangleShape mBackground;
	std::vector<std::pair<sf::VertexArray, sf::Transform>> _pauseText;
	std::vector<MenuItem> _pauseButtons;
};


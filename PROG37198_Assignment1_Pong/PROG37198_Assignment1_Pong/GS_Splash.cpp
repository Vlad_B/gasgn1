#include "GS_Splash.h"
#include "AssetManager.h"
#include "GameStateManager.h"
#include "GS_MainMenu.h"

#include <SFML/Graphics.hpp>

#include <iostream>

#include "MenuItem.h"

GS_Splash::GS_Splash()
	: 
	mCurrentFadeTime(0.0f),
	mCurrentlyFading(false), //false means unfading
	FADE_TIME(5.0f),
	TARGET_COLOR(255, 0, 255),
	_splashTextVelocity(1, 1)

{
}

GS_Splash::~GS_Splash()
{
}

void GS_Splash::init()
{
	//Text label
	AssetManager &assets = AssetManager::getInstance();
	_splashText.first = assets.getText("PRESS ENTER TO PLAY", 30);

	//position
	_splashText.second = sf::Transform();
	_splashTextPosition.x = SCREEN_WIDTH / 2 - _splashText.first.getBounds().width / 2;
	_splashTextPosition.y = 100.0f;
	_splashText.second.translate(_splashTextPosition);

	//color changing effect
	for (std::size_t i = 0; i < _splashText.first.getVertexCount(); ++i) {
		_splashText.first[i].color = TARGET_COLOR;
	}
	mCurrentFadeTime = 0.0f;
	mCurrentlyFading = false; //Start it positive so that it moves backwards on start

	/*Test Code*/
	//_startButton = MenuItem("Start", 56, sf::Color::Green);
	//_startButton.registerCallback(std::bind(&GS_Splash::startButton, this, std::placeholders::_1));
	
	// Group Members - Vlad Bronowicki, Colin Repas
	mGroupMembers.emplace_back(assets.getText("Group: Vlad Bronowicki, Colin Repas", 20), sf::Transform());
	mGroupMembers.back().second.translate(SCREEN_WIDTH * 0.5f - mGroupMembers.back().first.getBounds().width * 0.5f, SCREEN_HEIGHT * 0.65f);
	// Credits - music by John Peel, ring sfx by Microsoft
	mCredits.emplace_back(assets.getText("Song by: John Peel", 15), sf::Transform());
	mCredits.back().second.translate(SCREEN_WIDTH * 0.5f - mCredits.back().first.getBounds().width * 0.5f, SCREEN_HEIGHT * 0.8f);
	mCredits.emplace_back(assets.getText("Sfx by: Microsoft", 15), sf::Transform());
	mCredits.back().second.translate(SCREEN_WIDTH * 0.5f - mCredits.back().first.getBounds().width * 0.5f, SCREEN_HEIGHT * 0.9f);
}

void GS_Splash::deinit()
{
	_splashText.first.clear();

}

void GS_Splash::pause()
{
}

void GS_Splash::resume()
{
}

bool GS_Splash::handleEvent(sf::Event const &ev)
{

	if (ev.type == sf::Event::KeyReleased) {
		if (ev.key.code == sf::Keyboard::Enter) {
			GameStateManager::getInstance().swap(GS_MainMenu::getInstance());
		}
		else if (ev.key.code == sf::Keyboard::Escape) {
			GameStateManager::getInstance().pop();
		}
	}
	return true;
}

//Cool effect which makes the text move around the screen
void GS_Splash::update(float delta)
{
	_splashTextPosition += _splashTextVelocity;
	_splashText.second.translate(_splashTextVelocity);
	if (_splashTextPosition.x < 0 || _splashTextPosition.x > SCREEN_WIDTH - _splashText.first.getBounds().width) {
		_splashTextVelocity.x *= -1;
	}
	if (_splashTextPosition.y < 0 || _splashTextPosition.y > SCREEN_HEIGHT - _splashText.first.getBounds().height) {
		_splashTextVelocity.y *= -1;
	}


}

//renders the text 
void GS_Splash::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.clear(sf::Color::Blue);
	target.draw(_splashText.first, _splashText.second);
	//target.draw(_startButton);
	for (std::size_t i = 0; i < mGroupMembers.size(); ++i) {
		target.draw(mGroupMembers[i].first, mGroupMembers[i].second);
	}
	for (std::size_t i = 0; i < mCredits.size(); ++i) {
		target.draw(mCredits[i].first, mCredits[i].second);
	}
}

const void GS_Splash::startButton(MenuItem & item)
{
	//std::cout << "HELLO" << std::endl;
}
#pragma once
#include "AGameState.h"
#include <SFML/Graphics/RectangleShape.hpp>
#include <vector>
#include "MenuItem.h"
#include "MenuSlider.h"
class GS_Options: public AGameState
{
	//Singleton-a-fy
	GS_SINGLETON(GS_Options)
public:
		// Inherited via AGameState
	virtual void init() override;
	virtual void deinit() override;
	virtual void pause() override;
	virtual void resume() override;
	virtual bool handleEvent(sf::Event const &) override;
	virtual void update(float delta) override;
protected:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const override;
	std::vector<std::pair<sf::VertexArray, sf::Transform>> _optText;
	std::vector<MenuItem> _optButtons;
	std::vector<MenuSlider> _optSliders;

	void _musicUpBindMethod(MenuItem &caller);
	void _musicDownBindMethod(MenuItem &caller);

	void _sfxUpBindMethod(MenuItem &caller);
	void _sfxDownBindMethod(MenuItem &caller);


};


#pragma once

enum MenuItemState {
	MIS_NORMAL,
	MIS_HOVERED = 1,
	MIS_PRESSED = 2
};


#include "GameStateManager.h"
#include "AGameState.h"
#include "AssetManager.h"

#include <SFML/Graphics.hpp>

#include <iostream>

int GameStateManager::play()
{
	//Window/Library setup
	sf::Vector2u windowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	sf::Uint32 windowStyle =
#if _DEBUG
		sf::Style::Default;
#else
		sf::Style::Titlebar;
#endif
	sf::RenderWindow window(sf::VideoMode(windowSize.x, windowSize.y), "PONG", windowStyle);
	//Game Loop setup
	bool running = true;
	int const FRAME_SKIP = 5;
	float const DELTA = 1.0f / 30.0f;
	sf::Time gameTime, clockTime, deltaTime(sf::seconds(DELTA));
	sf::Clock clock;
	clock.restart();
	clockTime = clock.getElapsedTime();
	gameTime = clockTime;

	//Game loop
	while (running) {
		bool drawRequested = false;
		int frames = 0;
		//Events
		sf::Event ev;
		while (window.pollEvent(ev)) {
			switch (ev.type) {
			case sf::Event::Closed:
				running = false;
				break;
			default:
				handleEvent(ev);
			}
		} //while(window.pollEvent(ev)

		//mmm spaghetti
		if (!running) {
			break;
		}

		//Update game
		while (frames++ < FRAME_SKIP && (clockTime = clock.getElapsedTime()) > gameTime) {
			gameTime += deltaTime;
			drawRequested = true;
			update(DELTA);
		}

		//mmm even more spaghetti
		if (mGameStates.empty()) {
			running = !running;
			break;
		}

		//Debug message if game is running slow
#if _DEBUG
		if (frames >= FRAME_SKIP) {
			std::cout << "Frame Skip Required" << std::endl;
		}
#endif
		//Draw game
		if (drawRequested) {
			draw(window);
		}

	} // while(isRunning) 
	window.close();
	//Safe to clear the game states, since they should all be singletons.
	// In fact calling "delete" would be a baaaad idea.
	mGameStates.clear();
	return 0;
}

void GameStateManager::push(AGameState & newGameState)
{
	//pauses previous if applicable
	if (mGameStates.size() > 0) {
		mGameStates.back()->pause();
	}
	//calls init, then resume of new gs, then pushes 
	newGameState.init();
	newGameState.resume();
	mGameStates.push_back(&newGameState);
	//std::cout << "Size of mGameStates after push: " << mGameStates.size() << std::endl;
}

///Swap
/// - Pop current state
/// - Push new State
void GameStateManager::swap(AGameState & newGameState)
{
	this->pop();
	this->push(newGameState);
}


///Pop
/// - Pauses current state
/// - De-initializes current state
/// - Removes current state from stack
/// - Resumes new Current State if applicable
void GameStateManager::pop()
{
	if (!mGameStates.empty()) {
		mGameStates.back()->pause();
		mGameStates.back()->deinit();
	}
	mGameStates.pop_back();
	if (!mGameStates.empty()) {
		mGameStates.back()->resume();
	}

}

//calls pop on all states in the stack
void GameStateManager::popAll()
{
	while (mGameStates.size() > 0)
	{
		pop();
	}
}


//calls pop all, then pushes a new gs on the stack
void GameStateManager::popAllThenPush(AGameState & newGameState)
{

	this->popAll();
	this->push(newGameState);
}


void GameStateManager::handleEvent(sf::Event const & ev)
{
	if (!mGameStates.empty()) {
		mGameStates[mGameStates.size() - 1]->handleEvent(ev);
	}
}

void GameStateManager::update(float delta)
{
	if (!mGameStates.empty()) {
		mGameStates[mGameStates.size() - 1]->update(delta);
	}
}

///Clears the current Window of all renderings
///Loops through gamestates and draws draws them assuming the one above it isn't opaque
void GameStateManager::draw(sf::RenderWindow & window)
{

	window.clear();
	for (std::size_t i = mGameStates.size(); i > 0; --i) {
		if (mGameStates[i - 1]->isOpaque() || i - 1 == 0) {
			for (std::size_t j = i - 1; j < mGameStates.size(); ++j) {
				window.draw(*mGameStates[j]);
			}
			break;
		}
	}
	window.display();
}

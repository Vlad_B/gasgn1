#pragma once
#include "MenuItemState.h"

#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/VertexArray.hpp>

#include <string>
#include <vector>
#include <functional>

class MenuItem;

typedef std::function<void(MenuItem &)> cb_t;

class MenuItem : public sf::Drawable, public sf::Transformable
{
public:
	MenuItem(std::string text, float textHeight, sf::Color bgColor);
	MenuItem();
	virtual ~MenuItem();

	void setText(std::string newText, bool resize = false);

	void setDisabled(bool status);
	bool isDisabled() const;
	void handleEvents(sf::Event const &ev);
	void registerCallback(const cb_t &callback);
	void unregisterCallbacks();
	void setForcedState(bool force, int forcedState = MIS_NORMAL);

	float getWidth();
	float getHeight();

protected:
	// Inherited via Drawable
	virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const override;

private:
	float mTextHeight;
	bool mDisabled;
	bool mForced;
	int mForcedState;
	int mState;

	sf::FloatRect mBounds;
	std::vector<sf::VertexArray> mVertices;

	//Step 4a Add a collection for your registered callbacks here.
	typedef std::vector<cb_t> _cb_v;
	_cb_v _callbacks;
};

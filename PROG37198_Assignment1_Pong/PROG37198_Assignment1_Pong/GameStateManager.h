#pragma once

#include <vector>

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

//Forward declaration of the gamestate with event and window
//Needed because GameStateManager doesn't know about them at compile time
class AGameState;
namespace sf {
	class RenderWindow;
	class Event;
}

class GameStateManager
{
public:
	//Singleton implementation
	static GameStateManager &getInstance() {
		static GameStateManager instance;
		return instance;
	}
private: 
	GameStateManager() {}
	~GameStateManager() {};
	GameStateManager(GameStateManager const &);
	void operator=(GameStateManager const &);
public:
	int play();
	void push(AGameState &newGameState);
	void swap(AGameState &newGameState);
	void pop();
	void popAll();
	void popAllThenPush(AGameState &newGameState);

private:
	void handleEvent(sf::Event const &ev);
	void update(float delta);
	void draw(sf::RenderWindow &window);
	std::vector<AGameState *> mGameStates;
};


#include "MenuSlider.h"
#include "AssetManager.h"

#include <SFML/Graphics.hpp>
#include <algorithm>

#include <iostream>

//TODO - Refactor to make better use of height variable
MenuSlider::MenuSlider(float width, float height, sf::Color color)
	: MenuItem("", height / 2.0f, color)
{
	MenuItem::setForcedState(false);
	//slider button stuff
	lessButton = MenuItem("p", 30, color);
	lessButton.setForcedState(false);
	moreButton = MenuItem("m", 30, color);
	//private variable stuff
	_width = width;
	_height = height;
	_sliderValue = 0.5f;
	_slider = sf::RectangleShape(sf::Vector2f(10, 10));
	_slider.setFillColor(sf::Color::White);
	_slider.setPosition(height / 2 - 5, width / 2 - 5);

	//Event handler registration for defailt slider button behaviour
	moreButton.registerCallback(std::bind(&MenuSlider::_plus, this, std::placeholders::_1));
	lessButton.registerCallback(std::bind(&MenuSlider::_minus, this, std::placeholders::_1));
}

MenuSlider::~MenuSlider()
{
	//Test code attempting to resolve a bug
	//lessButton.unregisterCallbacks();
	//moreButton.unregisterCallbacks();
}

float MenuSlider::getSliderValue()
{
	return _sliderValue;
}

void MenuSlider::registerLessCallback(const cb_t & callback)
{
	lessButton.registerCallback(callback);
}

void MenuSlider::registerMoreCallback(const cb_t & callback)
{
	moreButton.registerCallback(callback);

}

void MenuSlider::unregisterCallbacks()
{
	MenuItem::unregisterCallbacks();
	lessButton.unregisterCallbacks();
	moreButton.unregisterCallbacks();

}

void MenuSlider::handleEvents(sf::Event const & ev)
{
	//Test code for an all encompasing slider listener not currently implemented
	//switch (ev.type) {
	//case sf::Event::MouseMoved:
	//	if (this->getTransform().transformRect(_bounds).contains((float)ev.mouseMove.x, (float)ev.mouseMove.y)) {
	//	}
	//	break;
	//}
	//MenuItem::handleEvents(ev);
	lessButton.handleEvents(ev);
	moreButton.handleEvents(ev);
}

//overriding Transformable set position 
void MenuSlider::setPosition(sf::Vector2f & position)
{
	//base
	sf::Transformable::setPosition(position);

	//setting bounds
	_bounds.left = lessButton.getPosition().x;
	_bounds.top = lessButton.getPosition().y;
	_bounds.width = _width + moreButton.getWidth();
	_bounds.height = moreButton.getHeight();
	
	//positioning of buttons, the less button on the left at 0,0, and the more button on the right equal to the width of the slider (excluding the buttons width, but starting from relative 0)
	lessButton.setPosition(this->getPosition());
	moreButton.setPosition(this->getPosition().x + _width, this->getPosition().y);
	
	//Crazyness to get the slider icon to show up in the center of the slider
	_slider.setPosition(this->getPosition().x + moreButton.getWidth() + (_width - moreButton.getWidth()) * _sliderValue - 5, this->getPosition().y + moreButton.getHeight() / 2 - 5);
}

void MenuSlider::setPosition(float x, float y)
{
	sf::Vector2f temp = sf::Vector2f(x, y);
	this->setPosition(temp);
}

float MenuSlider::getWidth()
{
	return _width + MenuItem::getWidth();
}

#pragma region Slider Callback bind methods



void MenuSlider::_plus(MenuItem & caller)
{
	if (_sliderValue < 0.951f)
		_sliderValue += 0.05f;
	else
	{
		_sliderValue = 1;
	}
	//more craziness
	_slider.setPosition(this->getPosition().x + moreButton.getWidth() + (_width - moreButton.getWidth()) * _sliderValue - 5, this->getPosition().y + moreButton.getHeight() / 2 - 5);
	//std::cout << "" << std::to_string(this->getSliderValue()) << std::endl;
}

void MenuSlider::_minus(MenuItem & caller)
{
	if (_sliderValue > 0.05)
		_sliderValue -= 0.05f;
	else
	{
		_sliderValue = 0.001f;
	}
	//more craziness
	_slider.setPosition(this->getPosition().x + moreButton.getWidth() + (_width - moreButton.getWidth()) * _sliderValue - 5, this->getPosition().y + moreButton.getHeight() / 2 - 5);
	//std::cout << "" << std::to_string(this->getSliderValue()) << std::endl;
}

#pragma endregion

void MenuSlider::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	//MenuItem::draw(target, states);
	target.draw(lessButton, states);
	target.draw(moreButton, states);
	target.draw(_slider, states);
}